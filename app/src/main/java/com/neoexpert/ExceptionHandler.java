package com.neoexpert;




import android.content.*;
import android.content.pm.*;
import android.os.*;
import com.neoexpert.tr4.*;
import java.io.*;
import java.text.*;
import java.util.*;

public class ExceptionHandler implements
java.lang.Thread.UncaughtExceptionHandler
{

	private final Thread.UncaughtExceptionHandler defaultUEH;

	private Context ctx;

    private int versionCode;
	
	public ExceptionHandler(Context context)
	{
		try
		{
			versionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
		}
		catch (PackageManager.NameNotFoundException ignored)
		{}
		this.ctx=context;
		this.defaultUEH=Thread.getDefaultUncaughtExceptionHandler();
	}

	public void uncaughtException(Thread thread, Throwable exception)
	{
		StringWriter stackTrace = new StringWriter();
		exception.printStackTrace(new PrintWriter(stackTrace));
        StringBuilder errorReport = new StringBuilder();
		String timeStamp = new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(Calendar.getInstance().getTime());
		errorReport.append("***********").append(timeStamp).append("************\n");
		errorReport.append("versionCode: ").append(versionCode).append("\n\n");
		errorReport.append("************ CAUSE OF ERROR ************\n\n");
		errorReport.append(stackTrace.toString());

		errorReport.append("\n************ DEVICE INFORMATION ***********\n");
		errorReport.append("Brand: ");
		errorReport.append(Build.BRAND);
		String LINE_SEPARATOR = "\n";
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Device: ");
		errorReport.append(Build.DEVICE);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Model: ");
		errorReport.append(Build.MODEL);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Id: ");
		errorReport.append(Build.ID);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Product: ");
		errorReport.append(Build.PRODUCT);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("\n************ FIRMWARE ************\n");
		errorReport.append("SDK: ");
		errorReport.append(Build.VERSION.SDK_INT);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Release: ");
		errorReport.append(Build.VERSION.RELEASE);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Incremental: ");
		errorReport.append(Build.VERSION.INCREMENTAL);
		errorReport.append(LINE_SEPARATOR);

		String p;
		
		p=Environment.getExternalStorageDirectory() +
				"/tr4/";
		
		File file = new File(p);
		if (!file.exists())
			if (!file.mkdir())
				return;
		file = new File(p + "crashlog.txt");
		if (BuildConfig.DEBUG)
			file = new File(p + "crashlog_admin.txt");
		//Toast.makeText(getApplicationContext(),file.toString(),Toast.LENGTH_LONG).show();
		try
		{
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
			out.print(errorReport.toString());
			out.close();
		}
		catch (IOException ignored)
		{}

		defaultUEH.uncaughtException(thread, exception);

		//   Intent intent = new Intent(myContext, ForceClose.class);
		//   intent.putExtra("error", errorReport.toString());
		//  myContext.startActivity(intent);

		// android.os.Process.killProcess(android.os.Process.myPid());
		//System.exit(10);

	}
	public  boolean isStoragePermissionGranted() {
		if (Build.VERSION.SDK_INT >= 23) {
			//ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
			return ctx.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
					== PackageManager.PERMISSION_GRANTED;
		}
		else { //permission is automatically granted on sdk<23 upon installation
			return true;
		}
	}

}
