package com.neoexpert;

import android.app.*;
import android.content.*;
import android.content.pm.*;
import android.content.res.*;
import android.graphics.*;
import android.text.*;
import com.neoexpert.tr4.*;
import java.io.*;
import java.util.*;

public class Util
{
	public static final long SECONDS_IN_A_HOUR = 3600;
	public static final long SECONDS_IN_A_DAY = 24 * SECONDS_IN_A_HOUR;

	
	public static boolean supportsEs2(Context context)
	{
        return getGLESVersion(context) >= 0x30000;
	}
	public static int getGLESVersion(Context context){
		final ActivityManager activityManager = 
			(ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		final ConfigurationInfo configurationInfo = 
			activityManager.getDeviceConfigurationInfo();

        return configurationInfo.reqGlEsVersion;
		
	}
	public static String getText(Context c, int r) throws IOException
	{
		Resources res = c.getResources();
		InputStream in_s = res.openRawResource(r);

		byte[] b = new byte[in_s.available()];
		in_s.read(b);
		return new String(b);

	}

	public static InputStream getLevelStream(Context c, int r) throws IOException
	{
		Resources res = c.getResources();
		return res.openRawResource(r);
	}
    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html)
	{
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N)
		{
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        }
		else
		{
            result = Html.fromHtml(html);
        }
        return result;
    }
    public static String toHtml(Spanned html)
	{
        String result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N)
		{
            result = Html.toHtml(html, Html.FROM_HTML_MODE_LEGACY);
        }
		else
		{
            //noinspection deprecation
            result = Html.toHtml(html);
        }
        return result;
    }
	public static int getColorFromId(String id)
	{
		if (id == null)
			return Color.WHITE;
		Random rnd;
		try
		{
			rnd = new Random(Integer.valueOf(id));
		}
		catch (NumberFormatException e)
		{
			return Color.WHITE;
		}
		int r,g,b;
		r = rnd.nextInt(256);
		g = rnd.nextInt(256);
		b = rnd.nextInt(256);
		if (r + g + b < 256)
		{
			r += 128;r %= 256;
			g += 128;g %= 256;
			b += 128;b %= 256;
		}
		return Color.rgb(r, g, b);
		//return 0xFFFF0000;
	}

	
}
