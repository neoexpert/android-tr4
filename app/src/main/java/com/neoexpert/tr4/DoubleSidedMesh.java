package com.neoexpert.tr4;
import android.opengl.*;
import com.neoexpert.tr4.shader.*;
import java.util.*;
import java.nio.*;

public class DoubleSidedMesh extends Object3D
{
	public DoubleSidedMesh(NeoShader shader,FloatBuffer vertices, ShortBuffer indices){
		super(shader,vertices,indices);
	}

	

	@Override
	public void draw()
	{
		GLES30.glUniformMatrix4fv(shader.mMMatrixHandle, 1, false, mModelMatrix, 0);
		GLES30.glDisable(GLES30.GL_CULL_FACE);
		GLES30.glDrawElements(GLES30.GL_TRIANGLES, drawOrder.capacity(),GLES20.GL_UNSIGNED_SHORT,drawOrder);
		GLES30.glEnable(GLES30.GL_CULL_FACE);
	}
	
}
