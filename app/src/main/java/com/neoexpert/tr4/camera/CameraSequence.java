package com.neoexpert.tr4.camera;
import com.neoexpert.tr4.level.room.*;
import java.util.*;

public class CameraSequence
{
	ArrayList<FlybyCamera> cams=new ArrayList<>();
	int index=0;

	public boolean isEmpty()
	{
		return cams.size()<2;
	}
	public void addCam(FlybyCamera c)
	{
		cams.add(c);
	}
	public GLFlybyCamera next(){
		if(index+1>=cams.size())
			return null;
		GLFlybyCamera c=
		new GLFlybyCamera(cams.get(index+1),cams.get(index));
		index++;
		return c;
	}
}
