package com.neoexpert.tr4.camera;
import com.neoexpert.tr4.level.room.*;
import java.util.*;

public class CameraParser
{
	public ArrayList<CameraSequence> css;
	public CameraParser(FlybyCamera cams[])
	{
		CameraSequence cs=null;
		byte current_sequence_index=-1;
		for (FlybyCamera c:cams)
		{
			if(current_sequence_index!=c.sequence){
				 cs=new CameraSequence();
				 css.add(cs);
				 current_sequence_index=c.sequence;
			}
			cs.addCam(c);
		}
	}
}
