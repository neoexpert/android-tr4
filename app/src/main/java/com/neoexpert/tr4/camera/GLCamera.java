package com.neoexpert.tr4.camera;
import android.opengl.*;

public class GLCamera
{
	public float eyeX = 49.0f;
	public float eyeY = 6.0f;
	public float eyeZ = 38f;

	// We are looking toward the distance
	public float lookX = 0.0f;
	public float lookY = 0.0f;
	public float lookZ = 5.0f;

	// Set our up vector. This is where our head would be pointing were we holding the camera.
	public float upX = 0.0f;
	public float upY = 1.0f;
	public float upZ = 0.0f;
	public final float[] mViewMatrix = new float[16];

	public synchronized void updateMatrix()
	{
		Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, eyeX+ lookX,eyeY+  lookY, eyeZ+ lookZ, upX, upY, upZ);
	}
}
