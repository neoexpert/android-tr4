package com.neoexpert.tr4.camera;
import com.neoexpert.tr4.level.room.*;

public class GLFlybyCamera
{
	public float eyeX;
	public float stepEyeX;
	
	public float eyeY;
	public float stepEyeY;
	
	public float eyeZ;
	public float stepEyeZ;
	
	public float lookAtX;
	public float stepLookAtX;
	
	public float lookAtY;
	public float stepLookAtY;
	
	public float lookAtZ;
	public float stepLookAtZ;
	float steps=100f;
	
	int pos=0;
	
	
	
	public GLFlybyCamera(FlybyCamera first, FlybyCamera next){
		if(first.sequence!=next.sequence)
			pos=100;
		eyeX=first.x;
		stepEyeX=((next.x-first.x))/steps;
		eyeY=first.y;
		stepEyeY=((next.y-first.y))/steps;
		eyeZ=first.z;
		stepEyeZ=((next.z-first.z))/steps;
		
		lookAtX=first.dx;
		stepLookAtX=((next.dx-first.dx))/steps;
		lookAtY=first.dy;
		stepLookAtY=((next.dy-first.dy))/steps;
		lookAtZ=first.dz;
		stepLookAtZ=((next.dz-first.dz))/steps;
	}
	
	public boolean update(){
		eyeX+=stepEyeX;
		eyeY+=stepEyeY;
		eyeZ+=stepEyeZ;
		
		lookAtX+=stepLookAtX;
		lookAtY+=stepLookAtY;
		lookAtZ+=stepLookAtZ;
		pos++;
		return pos>steps;
	}
}
