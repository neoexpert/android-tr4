package com.neoexpert.tr4;

import android.content.*;
import android.opengl.*;
import com.neoexpert.*;
import com.neoexpert.math.*;
import com.neoexpert.mesh.*;
import com.neoexpert.rendering.*;
import com.neoexpert.tr4.camera.*;
import com.neoexpert.tr4.level.*;
import com.neoexpert.tr4.level.room.*;
import com.neoexpert.tr4.mesh.*;
import com.neoexpert.tr4.shader.*;
import java.util.*;
import javax.microedition.khronos.egl.*;
import javax.microedition.khronos.opengles.*;

import android.opengl.Matrix;
import com.neoexpert.rendering.Camera;
import com.neoexpert.tr4.level.room.Portal;
import javax.microedition.khronos.egl.EGLConfig;

public class Renderer implements GLSurfaceView.Renderer 
{
	private final Frustum frustum=new Frustum();
	private volatile boolean loadTextures;
	private int mTextureDataHandle;
	private final float[] mVPMatrix = new float[16];
    //private final float[] mProjectionMatrix = new float[16];
   
	private int mWidth;

	private int mHeight;
	int frame=0;
	private volatile Level level;
	private static final int MAX_LIGHTS=128;
	private RoomShader[] roomShader=new RoomShader[MAX_LIGHTS];
	//private MeshShader[] meshShader=new MeshShader[MAX_LIGHTS];
	protected ALogger logger;

	private Context context;
	private ArrayList<GLDrawable> drawables=new ArrayList<>();
	private ArrayList<GLDrawable> tramsparent_drawables=new ArrayList<>();

	private boolean animateCamera;

	private RendererInterface ri;

	private TRRoom currentRoom;

	private short[] floorData;

	

	private float miny;

	public static NeoShader fshader;

	
	
	public Renderer(Context context,ALogger logger){
		this.logger=logger;
		this.context=context;
	}

	public void addDrawable(Object3D d)
	{
		synchronized(drawables){
			drawables.add(d);
		}
	}

	public void setEntity(int entityIndex)
	{
		com.neoexpert.tr4.level.room.Entity item=ri.setEntity(entityIndex);
		if (ri instanceof LevelRenderer)
		{
			cam.setPos(-item.x / 1024,
			-item.y / 1024,
			item.z / 1024);
			
			
		}
	}

	public void toggleFlybyCamera(boolean t)
	{
		animateCamera=t;
	}

	public synchronized void setRenderer(RendererInterface ri)
	{
		this.ri=ri;
		this.cam=ri.cam;
		//cam.setPos(400,0,-1500);
		cam.setUp(0,1,0);
		float aspect = (float) mWidth / mHeight;
		cam.perspective(aspect,(float)(53.13*(Math.PI/360.0)),Z_NEAR,Z_FAR);
		Matrix.perspectiveM(cam.projectionMatrix,0,(float)(53.13),aspect,Z_NEAR,Z_FAR);
		
		//Matrix.perspectiveM(cam.projectionMatrix, 0, 53.13f, aspect, Z_NEAR, Z_FAR);
		drawables=ri.getDrawables();
		tramsparent_drawables=ri.getTransparentDrawables();
		
		

        // this projection matrix is applied to object coordinates
        //no idea why 53.13f, it was used in another example and it worked.
        
		
		setRotX(ri.rotx);
		setRotY(ri.roty);
		if(ri instanceof LevelRenderer)
			setCurrentRoom(((LevelRenderer)ri).currentRoom);
		//DebugDrawables.portalTest(fshader);
	}

	private void setCurrentRoom(TRRoom room)
	{
		if(room==null)return;
		
		RoomInfo room_info=room.r.room_info;
		DebugDrawables.clear();
		
		for (Portal p:room.r.portals)
		{
			DrawablePortal pl=trPortalToDrawablePortal(room_info, p);
			MeshBuilder mb=pl.getMesh();
			LinedObject p1m=new LinedObject(fshader, mb.genVertices(), mb.genIndices());
			//DebugDrawables.addDrawable(p1m);
			
			TRRoom r=ri.getRoom(p.toRoom);
			
			for (Portal p2:r.r.portals)
			{
				if(ri.getRoom(p2.toRoom)==currentRoom)
					continue;
				DrawablePortal pl2=trPortalToDrawablePortal(r.r.room_info, p2);
				mb = pl2.getMesh();
				LinedObject p2m = new LinedObject(fshader, mb.genVertices(), mb.genIndices());
				//DebugDrawables.addDrawable(p2m);

				PortalFrustum fs=pl.genFrustum(pl2);
				mb = fs.buildLined();
				LinedObject fm = new LinedObject(fshader, mb.genVertices(), mb.genIndices());
				//DebugDrawables.addDrawable(fm);
				break;
			}
			break;
			
		}
		
		currentRoom=room;
		DebugDrawables.clear();
		currentRoom.buildPortalsMesh();
	}

	public static DrawablePortal trPortalToDrawablePortal(RoomInfo room_info,Portal p)
	{
		Vec3 p0=new Vec3();
		p0.x = (room_info.x + p.v0.x) / -1024.0f;
		p0.y = (p.v0.y) / -1024.0f;
		p0.z = ((room_info.z + p.v0.z) / 1024.0f);
		Vec3 p1=new Vec3();
		p1.x = (room_info.x + p.v1.x) / -1024.0f;
		p1.y = (p.v1.y) / -1024.0f;
		p1.z = ((room_info.z + p.v1.z) / 1024.0f);
		Vec3 p2=new Vec3();
		p2.x = (room_info.x + p.v2.x) / -1024.0f;
		p2.y = (p.v2.y) / -1024.0f;
		p2.z = ((room_info.z + p.v2.z) / 1024.0f);
		Vec3 p3=new Vec3();
		p3.x = (room_info.x + p.v3.x) / -1024.0f;
		p3.y = (p.v3.y) / -1024.0f;
		p3.z = ((room_info.z + p.v3.z) / 1024.0f);

		DrawablePortal pl=new DrawablePortal(
			p2,
			p1,
			p3,
			p0);
			return pl;
	}
	
    public void setLevel(Level level)
	{
		if(roomShader==null)
			return;
		synchronized (drawables)
		{
			drawables.clear();
		}
		synchronized(tramsparent_drawables){
			tramsparent_drawables.clear();
		}
		this.level=level;
		loadTextures=true;
		this.fbc=level.levelData.flybyCameras;
		if(fbc!=null)
		if(fbc.length>0){
			camsequence=0;
			c=new GLFlybyCamera(fbc[(camsequence)%fbc.length],fbc[(camsequence+1)%fbc.length]);
		}
		floorData=level.levelData.floorData;
	}
	@Override
	public void onSurfaceCreated(GL10 p1, EGLConfig p2)
	{
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(context));

		int[] res =new int[1];
		GLES30.glGetIntegerv(GLES30.GL_MAX_VERTEX_UNIFORM_COMPONENTS, res, 0);
        logger.log("GL_MAX_VERTEX_UNIFORM_COMPONENTS: "+res[0]);
		GLES30.glGetIntegerv(GLES30.GL_MAX_VERTEX_ATTRIBS, res, 0);
        logger.log("GL_MAX_VERTEX_ATTRIBS: "+res[0]);
		
		//set the clear buffer color to light gray.
        GLES30.glClearColor(0.0f, .0f, 0.0f, 1.0f);
        //initialize the cube code for drawing.

        //if we had other objects setup them up here as well.
		for (int i=0;i < MAX_LIGHTS;i++)
		{
			roomShader[i] = new RoomShader(context, i);
			//meshShader[i] = new MeshShader(context, i);
		}
		shader=roomShader[MAX_LIGHTS-1];
		fshader=new LinedShader(context);
		logger.log("surface created "+drawables.size());
		if(level!=null)
			loadTextures=true;
		if(oscc!=null)
			oscc.onSurdaceCreated();
		GLES30.glEnable(GLES30.GL_CULL_FACE);
		GLES30.glCullFace(GLES30.GL_FRONT);
	}

	@Override
	public void onSurfaceChanged(GL10 p1, int width, int height)
	{
		mWidth = width;
        mHeight = height;
        // Set the viewport
        GLES30.glViewport(0, 0, mWidth, mHeight);
       //Matrix.perspectiveM(mProjectionMatrix, 0, 53.13f, aspect, Z_NEAR, Z_FAR);
		
	}
	private FlybyCamera[] fbc;
	int camsequence=0;
	CameraSequence cs;
	CameraParser fc;
	GLFlybyCamera c;
	NeoShader shader;
	protected float[][] lights=new float[0][0];
	protected float[][] lightcolors=new float[0][0];
	
	public void onDrawFrame(GL10 glUnused) {
		if(loadTextures){
			mTextureDataHandle=TextureHelper.loadTRTexture(level.textures32,context,logger);
			//level.textures32=null;
			
			loadTextures=false;
		}
		if (currentRoom != null)
		{
			lights = currentRoom.lights;
			lightcolors = currentRoom.lightcolors;
			shader = roomShader[lights.length];
		}
		
		GLES30.glUseProgram(shader.mProgramObject);
		// get handle to shape's transformation matrix

		Renderer.checkGlError();
		for(int i=0;i<lights.length;i++){
			GLES30.glUniform3fv(shader.mLightPosUniform[i],1,lights[i],0);
			GLES30.glUniform3fv(shader.mLightColorUniform[i],1,lightcolors[i],0);
		}
		drawWorld();
		
	}
	private void drawWorld(){
		if (animateCamera){
			if(fbc!=null)
			if (fbc.length > 0){
				cam.lookAt(
				-c.eyeX / 1024f,
				-c.eyeY / 1024f,
				c.eyeZ / 1024f,

				-(-c.eyeX + c.lookAtX) / 1024f,
				-(-c.eyeY + c.lookAtY) / 1024f,
				(-c.eyeZ + c.lookAtZ) / 1024f);
				
				setCurrentRoom(ri.getRoom(fbc[(camsequence) % fbc.length].roomID));
				processFloorData();
				if (c.update())
				{
					camsequence++;
					c = new GLFlybyCamera(fbc[(camsequence) % fbc.length], fbc[(camsequence + 1) % fbc.length]);
				}

			}
		}

        // Clear the color buffer  set above by glClearColor.
        GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);

        //need this otherwise, it will over right stuff and the cube will look wrong!
        GLES30.glEnable(GLES30.GL_DEPTH_TEST);


		GLES30.glEnable(GLES20.GL_BLEND); 
		GLES30.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);



		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle);

        // Set the camera position (View matrix)  note Matrix is an include, not a declared method.
        //Matrix.setLookAtM(mViewMatrix, 0, 0,    0,   -3,    0f,    0f,    0f,    0f, 1.0f, 0.0f);

		//cam.updateMatrix();


        // Create a rotation and translation for the cube


        //move the cube up/down and left/right
        //Matrix.translateM(mRotationMatrix, 0, mTransX, mTransY, 0);

        //mangle is how fast, x,y,z which directions it rotates.
        //Matrix.rotateM(mRotationMatrix, 0, mAngle, 1.0f, 1.0f, 1.0f);

        // combine the model with the view matrix


        // combine the model-view with the projection matrix
        //Matrix.multiplyMM(mVPMatrix, 0, mProjectionMatrix, 0, mVPMatrix, 0);



        //change the angle, so the cube will spin.
        //mAngle+=.4;


		frame++;


//		synchronized (rooms)
//		{
//			for (GLDrawable t:rooms)
//				t.draw(mMVPMatrix);
//			
//		}

		//cam.updateMatrix();
		//Matrix.setLookAtM(cam.mViewMatrix, 0, eyeX, eyeY, eyeZ, eyeX + lookX, eyeY +  lookY, eyeZ + lookZ, upX, upY, upZ);
		//if(cam!=null)
		//Matrix.multiplyMM(mVPMatrix, 0, cam.projectionMatrix, 0, cam.viewMatrix, 0);
		synchronized (drawables)
		{
			for (GLDrawable t:drawables)
			{

				//move the cube up/down and left/right
				//Matrix.translateM(mRotationMatrix, 0, mTransX, mTransY, 0);

				//mangle is how fast, x,y,z which directions it rotates.
				//Matrix.rotateM(mRotationMatrix, 0, mAngle, 1.0f, 1.0f, 1.0f);

				// combine the model with the view matrix


				// combine the model-view with the projection matrix





				if (t.hasAddidiveBlending())
					GLES30.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE);
				else
					GLES30.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
				//t.draw(cam.viewProjMatrix);
			}

		}
		if(currentRoom!=null){
			frustum.build(cam);
//			for(Portal p:currentRoom.r.portals){
//				
//				if(frustum.intersects(p.center,p.radius))
//					((LevelRenderer)ri).rooms[p.toRoom].draw(cam.viewProjMatrix);
//			}
			currentRoom.drawPortals(cam.viewProjMatrix);
			currentRoom.draw(cam.viewProjMatrix);
		}
		synchronized (tramsparent_drawables)
		{
			for (GLDrawable t:tramsparent_drawables)
			{

				//move the cube up/down and left/right
				//Matrix.translateM(mRotationMatrix, 0, mTransX, mTransY, 0);

				//mangle is how fast, x,y,z which directions it rotates.
				//Matrix.rotateM(mRotationMatrix, 0, mAngle, 1.0f, 1.0f, 1.0f);

				if (t.hasAddidiveBlending())
					GLES30.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE);
				else
					GLES30.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
				t.draw(cam.viewProjMatrix);
			}

		}
		synchronized (this)
		{
			if (frustumMesh != null){
				//frustumMesh.draw(cam.viewProjMatrix);
				
			}
			DebugDrawables.draw(cam.viewProjMatrix);
		}
		
		if (cam != null)
		{
//			if (cam.eyeY > miny)
//			{
//				cam.eyeY -= 0.01;
//				if (cam.eyeY < miny)
//					cam.eyeY = miny;
//				cam.refreshViewMatrix();
//			}
//			if (cam.eyeY < miny)
//			{
//				cam.eyeY += 0.01;
//				if (cam.eyeY > miny)
//					cam.eyeY = miny;
//				cam.refreshViewMatrix();
//			}
			if (w)
			{
				cam.moveForwards(0.1f);
				//cam.eyeX += (cam.lookX) / 30;
				//cam.eyeZ += (cam.lookZ) / 30;
				processFloorData();
				//cam.refreshViewMatrix();
			}

			if (s)
			{
				cam.moveBackwards(0.1f);
				//cam.eyeX -= (cam.lookX) / 30;
				//cam.eyeZ -= (cam.lookZ) / 30;
				cam.refreshViewMatrix();
			}
			if (up)
			{
				cam.eyeY += 0.01;
				cam.refreshViewMatrix();
			}
			if (down)
			{
				cam.eyeY -= 0.01;
				cam.refreshViewMatrix();
			}
		}
    }

	private void processFloorData()
	{
		
		int off=
			((int)(-cam.eyeX) - (currentRoom.r.room_info.x) / 1024) * currentRoom.r.numZsectors
			+ ((int)cam.eyeZ - (currentRoom.r.room_info.z) / 1024);
		StringBuilder sb=new StringBuilder();
		sb.append("eyeY: "+(-cam.eyeY));
		sb.append("\n");
		sb.append("a_room:"+currentRoom.r.alternate_room);
		sb.append("\n");
		try
		{
			Sector sec=currentRoom.r.sectors[off];
			
			miny=(sec.floor*256f)/-1024f+0.5f;
			
			sb.append("" + (-off) + " floor:" + sec.floor);
			sb.append("\n");
			/*sb.append("roomBelow: "+sec.roomBelow);
			sb.append("\n");
			sb.append("roomAbove: "+sec.roomAbove);
			sb.append("\n");
			sb.append("yBottom: "+currentRoom.r.room_info.yBottom/-1024f);
			sb.append("\n");
			sb.append("y: "+currentRoom.r.room_info.y);
			sb.append("\n");
			*/
			if(sec.roomBelow!=255)
			{
				setCurrentRoom(ri.getRoom(sec.roomBelow));
			}
			
			loop:
			for(int i=0;true;i++){
				short fd=floorData[sec.fdIndex+i];
				if(fd==0)
					break;
				int func=fd & 0x001f;
				sb.append("fd:" + Integer.toBinaryString(fd));
				sb.append("\n");
				sb.append("function:    ");
				sb.append(Integer.toHexString(func));
				sb.append("\n");
				switch (func)
				{
					case 0x00:
						break;
					case 0x01:
						sb.append("portal to room:");
						sb.append(floorData[sec.fdIndex + i+1] & 0xFFFF);
						sb.append("\n");
						setCurrentRoom(((LevelRenderer)ri).rooms[floorData[sec.fdIndex + i+1] & 0xFFFF]);
						i++;
						break;
					case 0x02:
						i++;
						break;
					case 0x03:
						i++;
						break;
					case 0x04:
						i++;
						break;
					case 0x06:
						break;
					case 0x07:
					case 0x0B:
					case 0x0C:
						i++;
						break;
					case 0x08:
					case 0x0D:
					case 0x0E:
						i++;
						break;
					case 0x09:
					case 0x0F:
					case 0x10:
						i++;
						break;
					case 0x0A:
					case 0x11:
					case 0x12:
					i++;
						break;
					case 0x13:
						break;
					case 0x14:
						break;
					default:
					throw new RuntimeException(sb.toString()+"\nunknown function: "+Integer.toHexString(func)+" fd:"+Integer.toBinaryString(fd));
				}
				
				if ((fd & (1 << 15)) != 0)
				{
					break;
				}

			}
		}
		catch (IndexOutOfBoundsException e)
		{
			sb.append("ioo");
		}
		logger.seText(sb.toString());
	}
	
	
	
	protected Renderer.OnSurfaceCreatedCallback oscc;
	interface OnSurfaceCreatedCallback{
		void onSurdaceCreated();
	}
	public void setOnSurfaceCreatedCallBack(OnSurfaceCreatedCallback oscc){
		this.oscc=oscc;
	}
	public static final float Z_NEAR = 0.1f;
    public static final float Z_FAR = 5000;
	
	private float rotx;

	private float roty;
	Camera cam;
	
	
	protected boolean w;

	protected boolean up;

	protected boolean down;

	protected boolean s;
	
	
	public void setRotY(float rotx)
	{
		this.rotx=rotx;
		cam.rotate(rotx,roty);
		/*
		float sinroty= (float) Math.sin(roty);
		cam.lookX=(float)(Math.sin(rotx)*sinroty);
		cam.lookY=(float)Math.cos(roty);
		cam.lookZ=(float)(Math.cos(rotx)*sinroty);
		cam.refreshViewMatrix();*/
		//update();
	}


	public void setRotX(float roty)
	{
		this.roty=roty;
		cam.rotate(rotx,roty);
		
		/*float sinroty=(float) Math.sin(roty);
		cam.lookX=(float) (Math.sin(rotx)*sinroty);
		cam.lookY=(float)Math.cos(roty);
		cam.lookZ=(float)(Math.cos(rotx)*sinroty);
		cam.refreshViewMatrix();*/
		//update();
	}
	LinedObject frustumMesh;
	public void s(boolean s)
	{
		this.s=s;
		synchronized (this)
		{
			frustumMesh = new LinedObject();
			MeshBuilder mb=frustum.build(cam);
			frustumMesh.setShader(fshader);
			frustumMesh.setIndices(mb.genIndices());
			frustumMesh.setVertices(mb.genVertices());
		}
	}

	public void down(boolean down)
	{
		this.down=down;
	}

	public void w(boolean w)
	{
		this.w=w;
	}

	public void up(boolean up)
	{
		this.up=up;
	}


	
	
	public static void checkGlError() {
        int error;
        while ((error = GLES30.glGetError()) != GLES30.GL_NO_ERROR) {
            //Log.e(TAG, ": glError " + error);
            //throw new RuntimeException(  ": glError " + error);
        }
    }
	public RoomShader[] getRoomShader(){
		return roomShader;
	}
//	public MeshShader[] getMeshShader(){
//		return meshShader;
//	}
}
