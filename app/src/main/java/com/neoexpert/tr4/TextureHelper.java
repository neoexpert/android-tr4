package com.neoexpert.tr4;


import android.content.*;
import android.graphics.*;
import android.opengl.*;
import android.os.*;
import com.neoexpert.tr4.*;
import java.io.*;
import com.neoexpert.tr4.level.*;
import com.neoexpert.*;

public class TextureHelper
{

	public static int twidth;
	public static int loadTexture(final Context context, final int resourceId)
	{

		final int[] textureHandle = new int[1];

		GLES20.glGenTextures(1, textureHandle, 0);

		if (textureHandle[0] == 0)
		{
			throw new RuntimeException("Error generating texture name.");
		}

		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;	// No pre-scaling

		// Read in the resource
		final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId, options);

		// Bind to the texture in OpenGL
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

		// Set filtering
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

		// Load the bitmap into the bound texture.
		GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
		
		//GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, GLES30.GL_RGBA, GLES30.GL_UNSIGNED_BYTE, bitmap);
		
		// Recycle the bitmap, since its data has been loaded into OpenGL.
		bitmap.recycle();

		return textureHandle[0];
	}
	static int little2big(int i) {
		return (i&0xff)<<24 | (i&0xff00)<<8 | (i&0xff0000)>>8 | (i>>24)&0xff;
	}
	public static int loadTRTexture(Texture[] t, Context context,ALogger logger)
	{
		//int a=(int) Math.sqrt(t.length) + 1;

		//int w=a * 256;
		int w=(int)Math.sqrt(t.length)+1;
		twidth=w;
		Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
		Bitmap bmp = Bitmap.createBitmap(256*w, 256*w, conf); // this creates a MUTABLE bitmap

		
		for (int iy=0;iy < twidth;iy++)
		{
			for (int ix=0;ix < twidth;ix++)
			{
				int ti=iy*twidth+ix;
				if(ti>=t.length)
					break;
				Texture tt=t[ti];
				for (int x=0;x < 256;x++)
					for (int y=0;y < 256;y++)
					{
						int c=tt.pixels[y][x];
						bmp.setPixel(ix * 256 + x,  iy*256+y, c);
					}
			}
		}
			
		try
		{
			String path = Environment.getExternalStorageDirectory().toString();
			
			FileOutputStream out = new FileOutputStream(path+"/huhu.png");

			bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
			// PNG is a lossless format, the compression factor (100) is ignored
		}
		catch (FileNotFoundException e)
		{}
	
		final int[] textureHandle = new int[1];

		GLES20.glGenTextures(1, textureHandle, 0);

		if (textureHandle[0] == 0)
		{
			throw new RuntimeException("Error generating texture name.");
		}

		// Load the bitmap into the bound texture.
		
		
		
		// Bind to the texture in OpenGL
		GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, textureHandle[0]);
		
		Renderer.checkGlError();
		
		// Set filtering
		GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR_MIPMAP_LINEAR);
		Renderer.checkGlError();
		
		GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
		Renderer.checkGlError();
		GLUtils.texImage2D(GLES30.GL_TEXTURE_2D, 0, bmp, 0);
		GLES30.glGenerateMipmap(GLES30.GL_TEXTURE_2D);
		Renderer.checkGlError();
		// Recycle the bitmap, since its data has been loaded into OpenGL.
		bmp.recycle();

		return textureHandle[0];
	}
	
}
