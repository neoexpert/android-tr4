package com.neoexpert.tr4;
import android.content.*;
import android.opengl.*;
import android.util.*;
import com.neoexpert.*;
import com.neoexpert.tr4.camera.*;
import com.neoexpert.tr4.level.*;
import com.neoexpert.tr4.level.animation.*;
import com.neoexpert.tr4.level.animation.Animation;
import com.neoexpert.tr4.level.mesh.*;
import com.neoexpert.tr4.level.room.*;
import com.neoexpert.tr4.mesh.*;
import com.neoexpert.tr4.shader.*;
import java.util.*;
import javax.microedition.khronos.egl.*;
import javax.microedition.khronos.opengles.*;

import com.neoexpert.tr4.level.room.Entity;
import javax.microedition.khronos.egl.EGLConfig;
import java.nio.*;


public class LevelRenderer extends RendererInterface
{

	@Override
	public TRRoom getRoom(int id)
	{
		return rooms[id];
	}


	public TRRoom currentRoom;

	private RoomShader[] roomShader;

	//private MeshShader[] meshShader;

	public TRRoom[] rooms;

	@Override
	public Entity setEntity(int entityIndex)
	{
		Entity[] items=l.levelData.items;

		Model[] models=l.levelData.moveables;
		Model f=null;
		Entity item=items[entityIndex % items.length];
		int soffset=0;
		cam.setPos(-item.x/1024,-item.y/1024,item.z/1024);
		return item;
	}

	
	

	//private Level l;


	private Context context;



	private Level l;

	private int mTextureDataHandle;

	private Entity lara;
	

	
    //
    public LevelRenderer(Context context,ALogger logger,RoomShader[] roomShader) {
        super(logger);
		//cube can not be instianated here, because of "no egl context"  no clue.
        //do it in onSurfaceCreate and it is fine.  odd, but workable solution.
		this.context=context;
		this.roomShader=roomShader;
		//this.meshShader=meshShader;
		roty = -(float) Math.PI;
		rotx = (float) Math.PI / 2;
	
    }


	public void setLevel(Level l)
	{
		if(roomShader==null)
			return;
		//TextureHelper.loadTRTexture(l.texttiles32,context);
		this.l=l;
		int[] _at=l.levelData.animatedTextures;
		ObjectTexture[] ot=l.levelData.objectTextures;
		Set<Integer> at=new HashSet<>();
		for (int i=0;i < _at.length;i++)
		{
			at.add(_at[i]);
			logger.log("animated texture found:"+i);
		}
		synchronized(drawables){
			drawables.clear();
			tramsparent_drawables.clear();
			//rooms=new TRRoom[1];
			 rooms=new TRRoom[l.levelData.rooms.length];
			for(int i=0;i<l.levelData.rooms.length;i++){
				Room room=l.levelData.rooms[i];
				//if(room.isFilledWithWater())
					//continue;
				//logger.log("alternate_room: "+room.alternate_room);
				TRRoom trroom=new TRRoom(room, l, roomShader,context, logger,at);
				rooms[i]=trroom;
				if(room.alternate_room<1000){
					//logger.log("room_skipped");
					continue;
				}
				
				
				if(trroom.hasVertices())
					drawables.add(trroom);
				if(trroom.roomWater!=null)
					tramsparent_drawables.add(trroom.roomWater);
				if(trroom.transparency!=null)
					tramsparent_drawables.add(trroom.transparency);
				
				if(trroom.doubleSidedMesh!=null)
					drawables.add(trroom.doubleSidedMesh);
				if(trroom.doubleSidedTransparentMesh!=null)
					tramsparent_drawables.add(trroom.doubleSidedTransparentMesh);
				if(trroom.portalMesh!=null)
					tramsparent_drawables.add(trroom.portalMesh);
			}
			for(TRRoom r:rooms){
				r.parsePortals(rooms);
			}
		
			
			Entity[] items=l.levelData.items;

			Model[] models=l.levelData.moveables;
			Model f=null;
			
			for (int j=0;j<items.length;j++)
			{
				Entity item=items[j];
				f=null;
				//Entity item=l.levelData.lara;
				for (Model m:models)
				{
					if (m.id == item.typeID)
					{
						if(f!=null)
							throw new RuntimeException();
						f = m;
						
						break;
					}
				}
				if(f==null)continue;
				Animation a=l.levelData.animations[f.animation];
					
				a=l.levelData.animations[a.nextAnimation];
				float[][] rots=new float[f.numMeshes][3];
				Frame frame=l.levelData.getFrame(f.frameOffset,rots,f.numMeshes);
				
				//logger.log("xrot: "+rots[0][0]);
				//logger.log("yrot: "+rots[0][1]);
				//logger.log("zrot: "+rots[0][2]);
				
				//logger.log("xrot1: "+rots[1][0]);
				//logger.log("yrot1: "+rots[1][1]);
				//logger.log("zrot1: "+rots[1][2]);
				
				
				//logger.log("yoff: "+frame.offsetY);
				//logger.log("xoff: "+frame.offsetX);
				//logger.log("zoff: "+frame.offsetZ);
				
				//logger.log("y: "+item.y);
				//logger.log("x: "+item.x);
				//logger.log("z: "+item.z);
				try{
					//frame=l.levelData.getFrame(f.frameOffset);
					//logger.log("frame readen");
				}
				catch(Exception e){
					//logger.log("frammeofset err::"+ f.frameOffset/2);
				}
				//logger.log("angles:"+ frame.angles.length);
				BoundingBox bb=frame.bounding_box;
				if(bb.minx>bb.maxx)
					logger.log(bb.minx+">"+bb.maxx +" (x)");
				if(bb.miny>bb.maxy)
					logger.log(bb.miny+">"+bb.maxy+ " (y)");
				if(bb.minz>bb.maxz)
					logger.log(bb.minz+">"+bb.maxz+ " (z)");
					
					
				TRRoom r=rooms[item.room];
				
				//logger.log(f.numMeshes+"");
				//logger.log("meshes: "+l.levelData.meshes.size()+"");
				//logger.log("meshesp: "+l.levelData.meshPointers.length+"");
				///*
				
				Mesh mesh= l.levelData.meshes.get(l.levelData.meshPointers[f.startingMesh]);
				
				//logger.log(mesh.toString());
				
				//logger.log("meshtree:"+f.meshTree);
				TRMesh m=new TRMesh(mesh, l, context, logger);
				
				
				m.setShader(roomShader[r.lights.length]);
				int dx=0,dy=0,dz=0,mt;
				if (f.meshTree < l.levelData.meshTrees.length)
				{
					mt = l.levelData.meshTrees[f.meshTree];
					dx = l.levelData.meshTrees[f.meshTree + 1];
					dy = l.levelData.meshTrees[f.meshTree + 2];
					dz = l.levelData.meshTrees[f.meshTree + 3];
					//logger.log("dx:" + dx + "dy:" + dy + "dz:" + dz);
				}
				
				//logger.log("collradius:"+mesh.collRadius/1024f);
				m.translate((-item.x-frame.offsetX)/1024f, (-item.y-frame.offsetY)/1024f,(item.z+frame.offsetZ)/1024f);
				int angle=(item.angle / 16384) * -90;
				
				m.rotateY(angle+(int)rots[0][1]);
				m.rotateX(-(int)rots[0][0]);
				m.rotateZ(-(int)rots[0][2]);
				//mod.stack.push(m);
				TRMesh cp=m;
				TRMesh root_mesh=cp;
				if(m.hasAddidiveBlending())
					tramsparent_drawables.add(m);
				else
					r.addChild(m);
				Stack<TRMesh> stack=new Stack<>();
				TRMesh last_mesh=m;
				for (int i=1;i < f.numMeshes;i++)
				{
					
					mesh= l.levelData.meshes.get(l.levelData.meshPointers[f.startingMesh + i]);

					m=new TRMesh(mesh, l, context, logger);

					
					//logger.log(m+"");
					//int dx=0,dy=0,dz=0;
					
						mt = l.levelData.meshTrees[f.meshTree + 0 + (i-1) * 4];
						dx = l.levelData.meshTrees[f.meshTree + 1 + (i-1) * 4];
						dy = l.levelData.meshTrees[f.meshTree + 2 + (i-1) * 4];
						dz = l.levelData.meshTrees[f.meshTree + 3 + (i-1) * 4];
						//logger.log("mt"+Integer.toBinaryString(mt));
						
					
						//if (stack < 0)break;
						//logger.log(dy+"\n");
					

					

						
					if (((mt >> 0) & 1) == 1)
					{
						//stack--;
						cp=stack.pop();
					}
					else
						cp = last_mesh;
						
					if (((mt >> 1) & 1) == 1)
					{
						stack.push(cp);
						//stack++;
					}
					
					//m.translate(cp.x+ (-dx) / 1024f,cp.y+ ( -dy) / 1024f, cp.z+ (dz) / 1024f);
				
					m.setLocalTranslation((-dx) / 1024f, ( -dy) / 1024f, (dz) / 1024f);
					cp.addChild(m);
					last_mesh=m;
					//int angle=(item.angle / 16384) * -90;
					//logger.log(angle+"");
					//m.rotateY(angle);
					
					m.rotateY((int)rots[i][1]);
					m.rotateX(-(int)rots[i][0]);
					m.rotateZ(-(int)rots[i][2]);
					
					//if(m.hasAddidiveBlending())
						//tramsparent_drawables.add(m);
					//else
						//drawables.add(m);
				}
				float[] id=new float[16];
				Matrix.setIdentityM(id,0);
				//root_mesh.calcInverseBindTransform(id);
				//*/
				f=null;
			}
			lara = l.levelData.lara;
			cam.setPos(-lara.x/1024,
			-lara.y/1024+1,
			lara.z/1024);
			
			currentRoom=rooms[lara.room];
			//drawables.clear();
			//drawables.add(rooms[lara.room]);
			//drawables.add(rooms[24]);
			//drawables.add(rooms[4]);
		}
			
			
		logger.log("x:"+cam.eyeX);
		logger.log("y:"+cam.eyeY);
		logger.log("z:"+cam.eyeZ);
			/*
			eyeX=-60;
			eyeY=0;
			eyeZ=56;
			//*/
		
			
		
		
		
		//fc=new CameraParser(fbc);
		//cs=fc.cs[0];
		//c=cs.next();
		//c=new GLFlybyCamera(fbc[0],fbc[1]);
		
	
		System.gc();
	}

    ///
    // Create a shader object, load the shader source, and
    // compile the shader.
    //

    /**
     * Utility method for debugging OpenGL calls. Provide the name of the call
     * just after making it:
     *
     * <pre>
     * mColorHandle = GLES30.glGetUniformLocation(mProgram, "vColor");
     * MyGLRenderer.checkGlError("glGetUniformLocation");</pre>
     *
     * If the operation is not successful, the check throws an error.
     *
     * @param glOperation - Name of the OpenGL call to check.
     */
    

	

	
	
    ///
    // Initialize the shader and program object
    //
    

	int frame=0;
	
    // /
    // Draw a triangle using the shader pair created in onSurfaceCreated()
    //
	/*
    public void onDrawFrame(GL10 glUnused) {
		if(loadTextures){
			mTextureDataHandle=TextureHelper.loadTRTexture(l.texttiles32,context);
			loadTextures=false;
		}
		
//		if (fbc != null)
//		{
//			eyeX = -c.eyeX/1024f;
//			eyeY = -c.eyeY/1024f;
//			eyeZ = c.eyeZ/1024f;
//
//			lookX = -(-c.eyeX+ c.lookAtX)/1024f;
//			lookY = -(-c.eyeY+ c.lookAtY)/1024f;
//			lookZ = (-c.eyeZ+ c.lookAtZ)/1024f;
//			if(c.update()){
//				
//				
//				logger.log("from:"+fbc[(camsequence)%fbc.length]);
//				logger.log("to:"+fbc[(camsequence+1)%fbc.length]);
//				
//				c=new GLFlybyCamera(fbc[(camsequence)%fbc.length],fbc[(camsequence+1)%fbc.length]);
//				camsequence++;
////				c=cs.next();
////				if(c==null){
////					camsequence++;
////					cs=fc.cs[camsequence%fc.cs.length];
////					if(cs==null){
////						camsequence=0;
////						cs=fc.cs[camsequence%8];
////					}
////					c=cs.next();
////					
////				}
////				
//			}
//
//		}


        // Clear the color buffer  set above by glClearColor.
        GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);

        //need this otherwise, it will over right stuff and the cube will look wrong!
        GLES30.glEnable(GLES30.GL_DEPTH_TEST);

		
		GLES30.glEnable(GLES20.GL_BLEND); 
		GLES30.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
		
		
		
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle);
		
        // Set the camera position (View matrix)  note Matrix is an include, not a declared method.
        //Matrix.setLookAtM(mViewMatrix, 0, 0,    0,   -3,    0f,    0f,    0f,    0f, 1.0f, 0.0f);

		Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, eyeX+ lookX,eyeY+  lookY, eyeZ+ lookZ, upX, upY, upZ);
		
		
        // Create a rotation and translation for the cube
        

        //move the cube up/down and left/right
        //Matrix.translateM(mRotationMatrix, 0, mTransX, mTransY, 0);

        //mangle is how fast, x,y,z which directions it rotates.
        //Matrix.rotateM(mRotationMatrix, 0, mAngle, 1.0f, 1.0f, 1.0f);

        // combine the model with the view matrix
        

        // combine the model-view with the projection matrix
        //Matrix.multiplyMM(mVPMatrix, 0, mProjectionMatrix, 0, mVPMatrix, 0);

        

        //change the angle, so the cube will spin.
        //mAngle+=.4;
		
		
		frame++;
		
		
//		synchronized (rooms)
//		{
//			for (GLDrawable t:rooms)
//				t.draw(mMVPMatrix);
//			
//		}
		
		Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, eyeX + lookX, eyeY +  lookY, eyeZ + lookZ, upX, upY, upZ);
		Matrix.multiplyMM(mVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);
		synchronized (drawables)
		{
			for (GLDrawable t:drawables)
			{
				
				//move the cube up/down and left/right
				//Matrix.translateM(mRotationMatrix, 0, mTransX, mTransY, 0);

				//mangle is how fast, x,y,z which directions it rotates.
				//Matrix.rotateM(mRotationMatrix, 0, mAngle, 1.0f, 1.0f, 1.0f);
				
				// combine the model with the view matrix
				

				// combine the model-view with the projection matrix
				
				
				
				

				if (t.hasAddidiveBlending())
					GLES30.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE);
				else
					GLES30.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
				t.draw(mVPMatrix);
			}

		}
		synchronized (drawables)
		{
			for (GLDrawable t:tramsparent_drawables)
			{

				//move the cube up/down and left/right
				//Matrix.translateM(mRotationMatrix, 0, mTransX, mTransY, 0);

				//mangle is how fast, x,y,z which directions it rotates.
				//Matrix.rotateM(mRotationMatrix, 0, mAngle, 1.0f, 1.0f, 1.0f);
				
				if (t.hasAddidiveBlending())
					GLES30.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE);
				else
					GLES30.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
				t.draw(mVPMatrix);
			}

		}
		
		if(w){
			eyeX += (lookX) / 20;
			eyeZ += (lookZ) / 20;
		}
		
		if(s){
			eyeX -= (lookX) / 20;
			eyeZ -= (lookZ) / 20;
		}
		if(up){
			eyeY+=0.1;
		}
		if(down){
			eyeY-=0.1;
		}
    }
	*/

    // /
    // Handle surface changes
    //
    public void onSurfaceChanged(GL10 glUnused, int width, int height) {
           }

    //used the touch listener to move the cube up/down (y) and left/right (x)
    

}
