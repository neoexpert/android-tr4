package com.neoexpert.tr4.shader;

import android.opengl.*;
import android.util.*;
import com.neoexpert.*;
import java.io.*;

public abstract class NeoShader
{
	public int mProgramObject;

	public int mPositionHandle;
	public int mNormalHandle;
	public int mMVPMatrixHandle;
	public int mColorHandle;

	public int mTexCoordHandle;

	private int mTextureUniformHandle;

	public int mMMatrixHandle;

	public int[] mLightPosUniform;
	public int[] mLightColorUniform;
	private int lightsCount;
	public NeoShader(int lightsCount){
		this.lightsCount=lightsCount;
		mLightPosUniform=new int[lightsCount];
		mLightColorUniform=new int[lightsCount];
	}
	protected void init(){
		int vertexShader;
		int fragmentShader;
		int programObject;
		int[] linked = new int[1];

		String vs=getVertexShader();

		// Load the vertex/fragment shaders
		vertexShader = loadShader(GLES30.GL_VERTEX_SHADER, vs);

		StringBuilder fs=new StringBuilder();
		fs.append("#version 300 es");
		fs.append("\n");
		fs.append("#define LIGHTS_COUNT "+(lightsCount==0?1:lightsCount));
		fs.append("\n");
		fs.append(getFragmentShader());
		
		
		fragmentShader = loadShader(GLES30.GL_FRAGMENT_SHADER, fs.toString());



		// Create the program object
		programObject = GLES30.glCreateProgram();

		if (programObject == 0) {
			throw new RuntimeException();
		}

		GLES30.glAttachShader(programObject, vertexShader);
		GLES30.glAttachShader(programObject, fragmentShader);

		// Bind vPosition to attribute 0
		GLES30.glBindAttribLocation(programObject, 0, "vPosition");

		// Link the program
		GLES30.glLinkProgram(programObject);

		// Check the link status
		GLES30.glGetProgramiv(programObject, GLES30.GL_LINK_STATUS, linked, 0);

		if (linked[0] == 0) {
			StringBuilder sb=new StringBuilder();
			sb.append("Error linking program:\n");
			sb.append(GLES30.glGetProgramInfoLog(programObject));

			GLES30.glDeleteProgram(programObject);
			throw new RuntimeException(sb.toString());
		}

		// Store the program object
		mProgramObject = programObject;

		mPositionHandle =GLES30.glGetAttribLocation(mProgramObject, "aPosition");
		mColorHandle = GLES30.glGetAttribLocation(mProgramObject, "aColor");
		mTexCoordHandle =GLES30.glGetAttribLocation(mProgramObject, "aTexCoord");
		mNormalHandle = 	GLES30.glGetAttribLocation(mProgramObject, "aNormal");

		mMVPMatrixHandle = GLES30.glGetUniformLocation(mProgramObject, "uMVPMatrix");
		mMMatrixHandle=GLES30.glGetUniformLocation(mProgramObject, "uMMatrix");
			
		mTextureUniformHandle = GLES20.glGetUniformLocation(mProgramObject, "u_Texture");
		for(int i=0;i<lightsCount;i++){
			mLightPosUniform[i] = GLES20.glGetUniformLocation(mProgramObject, "light["+i+"].pos");
		}
		for(int i=0;i<lightsCount;i++){
			mLightColorUniform[i] = GLES20.glGetUniformLocation(mProgramObject, "light["+i+"].color");
		}
	}

	protected abstract String getVertexShader();
	protected abstract String getFragmentShader();
    public static int loadShader(int type, String shaderSrc) {
        int shader;
        int[] compiled = new int[1];

        // Create the shader object
        shader = GLES30.glCreateShader(type);

        if (shader == 0) {
            return 0;
        }
		
        // Load the shader source
        GLES30.glShaderSource(shader, shaderSrc);

        // Compile the shader
        GLES30.glCompileShader(shader);

        // Check the compile status
        GLES30.glGetShaderiv(shader, GLES30.GL_COMPILE_STATUS, compiled, 0);

        if (compiled[0] == 0) {
			StringBuilder sb=new StringBuilder();
			sb.append(GLES30.glGetShaderInfoLog(shader));
			sb.append("\n");
			sb.append(shaderSrc);
			
            throw new RuntimeException(sb.toString());
        }

        return shader;
    }
}
