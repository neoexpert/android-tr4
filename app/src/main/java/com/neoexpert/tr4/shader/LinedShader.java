package com.neoexpert.tr4.shader;

import android.content.*;
import com.neoexpert.*;
import com.neoexpert.tr4.*;
import java.io.*;

public class LinedShader extends NeoShader
{

	private Context context;
	public LinedShader(Context context){
		super(0);
		this.context=context;
		init();
	}
	@Override
	protected String getVertexShader()
	{
		try
		{
			return Util.getText(context, R.raw.color_vs);
		}
		catch (IOException e){
			throw new RuntimeException(e);
		}
	}

	@Override
	protected String getFragmentShader()
	{
		try
		{
			return Util.getText(context, R.raw.color_fs);
		}
		catch (IOException e){
			throw new RuntimeException(e);
		}

	}
	
}
