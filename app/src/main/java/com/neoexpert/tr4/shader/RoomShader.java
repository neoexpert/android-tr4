package com.neoexpert.tr4.shader;

import com.neoexpert.*;
import com.neoexpert.tr4.*;
import java.io.*;
import android.content.*;

public class RoomShader extends NeoShader
{
	private Context context;
	public RoomShader(Context context, int lightsCount){
		super(lightsCount);
		this.context=context;
		init();
	}
	@Override
	protected String getVertexShader()
	{
		try
		{
			return Util.getText(context, R.raw.room_vs);
		}
		catch (IOException e){
			throw new RuntimeException(e);
		}
	}

	@Override
	protected String getFragmentShader()
	{
		try
		{
			return Util.getText(context, R.raw.room_fs);
		}
		catch (IOException e){
			throw new RuntimeException(e);
		}
	
	}
	
}
