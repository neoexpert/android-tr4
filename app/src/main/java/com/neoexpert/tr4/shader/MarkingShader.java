package com.neoexpert.tr4.shader;

import android.content.*;
import android.opengl.*;
import com.neoexpert.*;
import com.neoexpert.tr4.*;
import java.io.*;

public class MarkingShader extends NeoShader
{

	private Context context;

	public int markColorHandle;
	public MarkingShader(Context context,int lightsCount){
		super(lightsCount);
		this.context=context;
		init();
		markColorHandle = GLES30.glGetUniformLocation(mProgramObject, "markColor");
	}
	@Override
	protected String getVertexShader()
	{
		try
		{
			return Util.getText(context, R.raw.markable_vs);
		}
		catch (IOException e){
			throw new RuntimeException(e);
		}
	}

	@Override
	protected String getFragmentShader()
	{
		try
		{
			return Util.getText(context, R.raw.markable_fs);
		}
		catch (IOException e){
			throw new RuntimeException(e);
		}

	}
	
}
