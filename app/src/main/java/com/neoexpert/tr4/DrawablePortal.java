package com.neoexpert.tr4;
import com.neoexpert.math.*;

import java.util.*;

public class DrawablePortal extends Portal
{
	private List<Object3D> children=new ArrayList<>();
	public DrawablePortal(Vec3 v0, Vec3 v1, Vec3 v2, Vec3 v3) {
		super(v0,v1,v2,v3);
	}

	public void removeDrawable(Object3D o)
	{
		children.remove(o);
	}

	public void draw(float[] mvpMatrix)
	{
		for(Object3D o:children){
			o.draw(mvpMatrix);
		}
	}

	public boolean contains(Object3D child){
		return children.contains(child);
	}
	public void addChild(Object3D child)
	{
		children.add(child);
	}
}
