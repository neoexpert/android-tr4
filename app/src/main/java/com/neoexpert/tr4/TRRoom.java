package com.neoexpert.tr4;

import android.content.*;
import android.opengl.*;
import com.neoexpert.*;
import com.neoexpert.math.*;
import com.neoexpert.mesh.*;
import com.neoexpert.tr4.level.*;
import com.neoexpert.tr4.level.mesh.*;
import com.neoexpert.tr4.level.room.*;
import com.neoexpert.tr4.mesh.*;
import com.neoexpert.tr4.shader.*;
import java.nio.*;
import java.util.*;
import com.neoexpert.tr4.PortalFrustum;

public class TRRoom extends Object3D
{
	public DoubleSidedMesh roomWater;

	public Object3D doubleSidedMesh;

	public DoubleSidedMesh doubleSidedTransparentMesh;

	public Room r;

	public DoubleSidedMesh portalMesh;

	private Level level;

	public void addChild(TRMesh m)
	{
		//m.lights=lights;
		//m.lightcolors=lightcolors;
		children.add(m);
	}

	public void addTransparentChild(TRMesh m)
	{
		//m.lights=lights;
		//m.lightcolors=lightcolors;
		transparent_children.add(m);
	}

	
    private static class RoomWater extends Object3D
	{
		public RoomWater(NeoShader shader,FloatBuffer mVertices, ShortBuffer mIndices){
			super(shader,mVertices,mIndices);
		}
		@Override
		public void draw()
		{
			GLES30.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE);
			GLES30.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.capacity(), GLES20.GL_UNSIGNED_SHORT, drawOrder);
			
		}
	}
	public TRRoom.Transparency transparency;
	private static class Transparency extends Object3D
	{

		@Override
		public void draw()
		{
			GLES30.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE);
			GLES30.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.capacity(), GLES20.GL_UNSIGNED_SHORT, drawOrder);
			
		}
		
		public Transparency(NeoShader shader,FloatBuffer mVertices, ShortBuffer mIndices){
			super(shader,mVertices,mIndices);
		}
	}

	private Context context;

	

	private ALogger logger;

	private boolean hasVertices;
    private ArrayList<GLDrawable> transparent_children=new ArrayList<>();
    private ArrayList<GLDrawable> children=new ArrayList<>();
	protected float[][] lights=new float[0][0];
	protected float[][] lightcolors=new float[0][0];
	
	
	public TRRoom(Room r,Level l,RoomShader[] shaders, Context ctx,ALogger logger,Set<Integer> at) {
		this.r=r;
		this.level=l;
		boolean iswaterroom=r.isFilledWithWater();
		int ytop=r.room_info.yTop;
		this.logger = logger;
		this.context = ctx;
		
		RoomInfo room_info=r.room_info;

	
		RoomVertex[] verts=r.roomVertices;
		Face4[] rects=r.rects;
		Face3[] triangles=r.triangles;

		int rectscount=1;
		int tricount=0;
		rectscount=rects.length;
		tricount=triangles.length;
		//int meshSize=(rectscount*4+3*tricount)*(3+4+2);
		
		
		MeshBuilder roomMesh=new MeshBuilder();
		MeshBuilder waterMesh=null;
		MeshBuilder transparencyMesh=null;
		MeshBuilder doubleSidedTMPMesh=null;
		MeshBuilder doubleSidedTransparentTMPMesh=null;
		
		
		
		//logger.log("room_lights: "+lights.length);
		Set<TRLight> lights=new HashSet<>();
		parseLights(r,lights,4);
		
		NeoShader shader=shaders[lights.size()];
		//MeshShader meshShader=meshShaders[lights.size()];
		setShader(shader);
		//buildPortalsMesh(room_info,r.portals);
		
		
		this.lights=new float[lights.size()][3];
		this.lightcolors=new float[lights.size()][3];
		int i=0;
		for(TRLight tr4l:lights){
			this.lights[i][0]=tr4l.x/-1024f;
			this.lights[i][1]=tr4l.y/-1024f;
			this.lights[i][2]=tr4l.z/1024f;
			lightcolors[i][0]=tr4l.color.r/255f;
			lightcolors[i][1]=tr4l.color.g/255f;
			lightcolors[i][2]=tr4l.color.b/255f;
			i++;
			
		}
		
		int twidth=(int)Math.sqrt(l.textures32.length) + 1;

		

		for(int j=0;j<rectscount;j++){
			
			Face4 f=rects[j];
			ObjectTexture ot=l.levelData.objectTextures[f.getTexture()];
			
			
			int t=ot.getTile();
			int tx=t%twidth;
			int ty=t/twidth;
			
			
			MeshBuilder currentMesh=null;
			
			if (ot.attr == 512)
			{
				//if texture is transparent and animated and the mesh is parallel to XZ plane, it is probably water
				if (at.contains(f.getTexture()) && verts[f.v0].vertex.y == verts[f.v1].vertex.y && verts[f.v1].vertex.y == verts[f.v2].vertex.y && verts[f.v2].vertex.y == verts[f.v3].vertex.y)
				{
					if(waterMesh==null)
						waterMesh=new MeshBuilder();
					currentMesh = waterMesh;
				
				}
				

			}
			if (currentMesh == null)
				if (ot.attr == 512&& f.isDoubleSided())
				{
					if (doubleSidedTransparentTMPMesh == null)
						doubleSidedTransparentTMPMesh = new MeshBuilder();

					currentMesh = doubleSidedTransparentTMPMesh;
				}
			if (currentMesh == null)
				if (f.isDoubleSided())
				{
					if (doubleSidedTMPMesh == null)
						doubleSidedTMPMesh = new MeshBuilder();

					currentMesh = doubleSidedTMPMesh;
				}
			if (currentMesh == null)
			{
				if (roomMesh == null)
					roomMesh = new MeshBuilder();
				hasVertices=true;
				currentMesh = roomMesh;
			}
			
			
			//v0
			currentMesh.addIndex((short)(currentMesh.fi+0));

			
			currentMesh.addIndex((short)(currentMesh.fi+1));

			
			currentMesh.addIndex((short)(currentMesh.fi+2));

			
			currentMesh.addIndex((short)(currentMesh.fi+0));

			
			currentMesh.addIndex((short)(currentMesh.fi+2));

			
			currentMesh.addIndex((short)(currentMesh.fi+3));

			
			currentMesh.fi+=4;

				
			//XYZ
			Vec3 pos=new Vec3();
			pos.x=(room_info.x + verts[f.v0].vertex.x) / -1024.0f;
			pos.y=( -verts[f.v0].vertex.y)/1024.0f;
			pos.z=((room_info.z+verts[f.v0].vertex.z)/1024.0f);
			currentMesh.addPosition(pos);
			
			//color rgba
			float avgl=((verts[f.v0].getR() +verts[f.v0].getG() + verts[f.v0].getB())/3f)/31f;
			
			Vec4 color=new Vec4();
			color.x= avgl;//verts[f.v0].getR() / 31.0f;
			
			color.y=(avgl);// verts[f.v0].getG() / 31.0f;
			
			color.z=(avgl);// verts[f.v0].getB() / 31.0f;
			
			color.w=(1.0f);
			currentMesh.addColor(color);
			

//			//textcoord
			Vec2 texcoord=new Vec2();
			texcoord.x=((tx*256+(ot.v0.xPixel))/(twidth*256.0f));
			
			texcoord.y=((ty*256+(ot.v0.yPixel))/(twidth*256.0f));
			currentMesh.addTexCoord(texcoord);
			

			pos=new Vec3();
			//XYZ
			pos.x=((room_info.x+verts[f.v1].vertex.x)/-1024.0f);
			
			pos.y=(( -verts[f.v1].vertex.y)/1024.0f);
			
			pos.z=((room_info.z+verts[f.v1].vertex.z)/1024.0f);
			currentMesh.addPosition(pos);
			
			//color rgba
			avgl=((verts[f.v1].getR() +verts[f.v1].getG() + verts[f.v1].getB())/3f)/31f;
			
			color=new Vec4();
			color.x=(avgl); // verts[f.v1].getR()/31.0f;
			
			color.y=(avgl);// verts[f.v1].getG()/31.0f;
			
			color.z=(avgl);// verts[f.v1].getB()/31.0f;
			
			color.w=(1.0f);
			currentMesh.addColor(color);
//			//textcoord
			texcoord=new Vec2();
			texcoord.x=((tx*256+(ot.v1.xPixel))/(twidth*256.0f));
			
			texcoord.y=((ty*256+(ot.v1.yPixel))/(twidth*256.0f));
			currentMesh.addTexCoord(texcoord);

			pos=new Vec3();
			//XYZ
			pos.x=((room_info.x+verts[f.v2].vertex.x)/-1024.0f);
			
			pos.y=(( - verts[f.v2].vertex.y)/1024.0f);
			
			pos.z=((room_info.z+verts[f.v2].vertex.z)/1024.0f);
			currentMesh.addPosition(pos);
			
			//color rgba
			avgl=((verts[f.v2].getR() +verts[f.v2].getG() + verts[f.v2].getB())/3f)/31f;
			color=new Vec4();
			color.x=(avgl); //verts[f.v2].getR()/31.0f;
			
			color.y=(avgl); //verts[f.v2].getG()/31.0f;
			
			color.z=(avgl); //verts[f.v2].getB()/31.0f;
			
			color.w=(1.0f);
			currentMesh.addColor(color);
			
			//textcoord
			texcoord=new Vec2();
			texcoord.x=((tx*256+(ot.v2.xPixel))/(twidth*256.0f));
			
			texcoord.y=((ty*256+(ot.v2.yPixel))/(twidth*256.0f));
			currentMesh.addTexCoord(texcoord);

			//XYZ
			pos=new Vec3();
			pos.x=((room_info.x+verts[f.v3].vertex.x)/-1024.0f);
			
			pos.y=(( -verts[f.v3].vertex.y)/1024.0f);
			
			pos.z=((room_info.z+verts[f.v3].vertex.z)/1024.0f);
			currentMesh.addPosition(pos);
			
			//color rgba
			avgl=((verts[f.v3].getR() +verts[f.v3].getG() + verts[f.v3].getB())/3f)/31f;
			
			color=new Vec4();
			color.x=(avgl); // verts[f.v3].getR()/31.0f;
			
			color.y=(avgl);// verts[f.v3].getG()/31.0f;
			
			color.z=(avgl);// verts[f.v3].getB()/31.0f;
			
			
			color.w=(1.0f);
			currentMesh.addColor(color);
			
		
			//textcoord
			texcoord=new Vec2();
			texcoord.x=((tx*256+(ot.v3.xPixel))/(twidth*256.0f));
			
			texcoord.y=((ty*256+(ot.v3.yPixel))/(twidth*256.0f));
			currentMesh.addTexCoord(texcoord);
		}
	
		
		//fi=0;
		for(int j=0;j< tricount;j++){

			Face3 f=triangles[j];
			ObjectTexture ot=l.levelData.objectTextures[f.getTexture()];
			int t=ot.getTile();
			int tx=t%twidth;
			int ty=t/twidth;
			MeshBuilder currentMesh=roomMesh;


			if (ot.attr == 512)
			{
				//if texture is transparent and animated and the mesh is parallel to XZ plane, it is probably water
				if (at.contains(f.getTexture()) && verts[f.v0].vertex.y == verts[f.v1].vertex.y && verts[f.v1].vertex.y == verts[f.v2].vertex.y)
				{
					if(waterMesh==null)
						waterMesh=new MeshBuilder();
					currentMesh = waterMesh;

				}


			}
			if ( currentMesh == null)
				if (ot.attr == 512&& f.isDoubleSided())
				{
					if (doubleSidedTransparentTMPMesh == null)
						doubleSidedTransparentTMPMesh = new MeshBuilder();

					currentMesh = doubleSidedTransparentTMPMesh;
				}
			if (currentMesh == null)
				if (f.isDoubleSided())
				{
					if (doubleSidedTMPMesh == null)
						doubleSidedTMPMesh = new MeshBuilder();

					currentMesh = doubleSidedTMPMesh;
				}
			if (currentMesh == null)
			{
				if (roomMesh == null)
					roomMesh = new MeshBuilder();
				hasVertices=true;
				currentMesh = roomMesh;
			}	
			
			//v0
			
			currentMesh.addIndex((short)(currentMesh.fi+0));

			
			currentMesh.addIndex((short)(currentMesh.fi+1));

			
			currentMesh.addIndex((short)(currentMesh.fi+2));

			
			currentMesh.fi+=3;



			//XYZ
			Vec3 pos=new Vec3();
			pos.x=((room_info.x+verts[f.v0].vertex.x)/-1024.0f);
			
			pos.y=(( -verts[f.v0].vertex.y)/1024.0f);
			
			pos.z=((room_info.z+verts[f.v0].vertex.z)/1024.0f);
			currentMesh.addPosition(pos);
			
			//color argb
			Vec4 color=new Vec4();
			float avgl=((verts[f.v0].getR() + verts[f.v0].getG() + verts[f.v0].getB()) / 3f) / 31f;
			color.x=(avgl);// //verts[f.v0].getR() / 31.0f;
			
			color.y=(avgl);// verts[f.v0].getG()/31.0f;
			
			color.z=(avgl);// verts[f.v0].getB()/31.0f;
			
			color.w=(1.0f);
			currentMesh.addColor(color);
//			
//			//textcoord
			Vec2 texcoord=new Vec2();
			texcoord.x=((tx * 256 + (ot.v0.xPixel)) / (twidth * 256.0f));

			texcoord.y=((ty*256+(ot.v0.yPixel))/(twidth*256.0f));
			currentMesh.addTexCoord(texcoord);
			
			//XYZ
			pos=new Vec3();
			pos.x=((room_info.x+verts[f.v1].vertex.x)/-1024.0f);
			
			pos.y=(( -verts[f.v1].vertex.y)/1024.0f);
			
			pos.z=((room_info.z+verts[f.v1].vertex.z)/1024.0f);
			
			currentMesh.addPosition(pos);
			//color argb
			avgl=((verts[f.v1].getR() + verts[f.v1].getG() + verts[f.v1].getB()) / 3f) / 31f;
			
			color=new Vec4();
			color.x=(avgl); //verts[f.v1].getR()/31.0f;
			
			color.y=(avgl);// verts[f.v1].getG()/31.0f;
			
			color.z=(avgl);// verts[f.v1].getB()/31.0f;
			
			color.w=(1.0f);
			currentMesh.addColor(color);
			
//			//textcoord
			texcoord=new Vec2();
			texcoord.x=((tx*256+(ot.v1.xPixel))/(twidth*256.0f));
			
			texcoord.y=((ty*256+(ot.v1.yPixel))/(twidth*256.0f));
			currentMesh.addTexCoord(texcoord);
			
			//XYZ
			pos=new Vec3();
			pos.x=((room_info.x+verts[f.v2].vertex.x)/-1024.0f);
			
			pos.y=(( - verts[f.v2].vertex.y)/1024.0f);
			
			pos.z=((room_info.z+verts[f.v2].vertex.z)/1024.0f);
			currentMesh.addPosition(pos);
			
			//color argb
			color=new Vec4();
			avgl=((verts[f.v2].getR() + verts[f.v2].getG() + verts[f.v2].getB()) / 3f) / 31f;
			color.x=(avgl);// verts[f.v2].getR()/31.0f;
			
			color.y=(avgl);// verts[f.v2].getG()/31.0f;
			
			color.z=(avgl);// verts[f.v2].getB()/31.0f;
			
			color.w=(1.0f);
			currentMesh.addColor(color);
			
//			//textcoord
			texcoord=new Vec2();
			texcoord.x=((tx*256+(ot.v2.xPixel))/(twidth*256.0f));
			
			texcoord.y=((ty*256+(ot.v2.yPixel))/(twidth*256.0f));
			currentMesh.addTexCoord(texcoord);

			
		}
		
     	if (roomMesh != null){
			roomMesh.calculateNormals();
			setVertices(roomMesh.genVertices());
			setIndices(roomMesh.genIndices());
		}
		
		
		
		if (transparencyMesh!=null){
			transparencyMesh.calculateNormals();
			transparency = new Transparency(shader,transparencyMesh.genVertices(),transparencyMesh.genIndices());
		}
		if (waterMesh!=null){
			waterMesh.calculateNormals();
			roomWater = new DoubleSidedMesh(shader,waterMesh.genVertices(),waterMesh.genIndices());		
			roomWater.setAdditiveBlending(true);
		}
		if (doubleSidedTMPMesh!=null){
			doubleSidedTMPMesh.calculateNormals();
			this.doubleSidedMesh = new DoubleSidedMesh(shader,doubleSidedTMPMesh.genVertices(),doubleSidedTMPMesh.genIndices());		
		}
		if (doubleSidedTransparentTMPMesh!=null){
			doubleSidedTransparentTMPMesh.calculateNormals();
			this.doubleSidedTransparentMesh = new DoubleSidedMesh(shader,doubleSidedTransparentTMPMesh.genVertices(),doubleSidedTransparentTMPMesh.genIndices());		
			doubleSidedTransparentMesh.setAdditiveBlending(true);
		}
        //setup the shaders
        
        // Store the program object
        
        //now everything is setup and ready to draw.
		for (RoomStaticMesh rsm: r.staticMeshes){
			StaticMesh smf=null;
			for (StaticMesh sm:l.levelData.staticMeshes)
			{
				if (rsm.meshID == sm.id)
					smf = sm;
			}

			Mesh mesh= l.levelData.meshes.get(l.levelData.meshPointers[smf.mesh]);

			TRMesh m=new TRMesh(mesh, l, context, logger);
			m.setShader(shader);
			//m.lights=this.lights;
			//m.lightcolors=this.lightcolors;
			m.translate(-rsm.x / 1024f, -rsm.y / 1024f, rsm.z / 1024f);


			int angle=(rsm.rotation / 16384) * -90;
			//logger.log(angle+"");
			m.rotateY(angle);
			if(m.hasAddidiveBlending())
				transparent_children.add(m);
			else
				children.add(m);
		}
    }

	private List<DrawablePortal> portals=new ArrayList<>();
	public void parsePortals(TRRoom[] rooms)
	{
		RoomInfo ri=r.room_info;
		for (com.neoexpert.tr4.level.room.Portal trp:r.portals)
		{
			DrawablePortal p1=Renderer.trPortalToDrawablePortal(ri, trp);
			portals.add(p1);
			TRRoom trRoom=rooms[trp.toRoom];
			
			p1.addChild(trRoom);
			for (com.neoexpert.tr4.level.room.Portal trp2:trRoom.r.portals)
			{
				TRRoom child=rooms[trp2.toRoom];
				if(child==this)
					continue;
				DrawablePortal p2=Renderer.trPortalToDrawablePortal(trRoom.r.room_info, trp2);
				TRRoom trRoom2=rooms[trp2.toRoom];
				if(p1.contains(trRoom2))
					continue;
				p1.addChild(trRoom2);
				if (!p1.isVisibleFrom(p2))
					continue;
				PortalFrustum fs=p1.genFrustum(p2);
				trRoom2.parsePortal(p1,fs,rooms);
			}
			//p1.removeDrawable(this);
		

		}
	}

	private void parsePortal(DrawablePortal target, PortalFrustum fs,TRRoom[] rooms)
	{
		for(com.neoexpert.tr4.level.room.Portal p:r.portals){
			DrawablePortal dp=Renderer.trPortalToDrawablePortal(r.room_info, p);
			if(fs.portal.isVisibleFrom(dp)){
				TRRoom room=rooms[p.toRoom];
				if(target.contains(room))
					continue;
				target.addChild(room);
				PortalFrustum fs2=fs.reduceTo(dp);
				if(fs2==null)continue;
				room.parsePortal(target,fs2,rooms);
			}
		}
	}

	private void parseLights(Room r,Set<TRLight> lights,int depth)
	{
		if(depth<=0)
			return;
		for(com.neoexpert.tr4.level.room.Portal p:r.portals){
			parseLights(level.levelData.rooms[p.toRoom],lights,depth-1);
		}
		TRLight[] trlights=r.lights;
		for(TRLight light:trlights){
			switch (light.type)
			{
				case TRLight.SUN:
					StringBuilder sb=new StringBuilder();
					sb.append("sun found: ");
					sb.append("dx: ");
					sb.append(light.dx / -1024f);
					sb.append(" dy: ");
					sb.append(light.dy / -1024f);
					sb.append(" dz: ");
					sb.append(light.dz / 1024f);
					sb.append("\n");
					
					
					//logger.log(sb.toString());
					break;
				case TRLight.LIGHT:
					lx = light.x / -1024f;
					ly = light.y / -1024f;
					lz = light.z / 1024f;
					lights.add(light);
					break;
			}
		}
	}

	public void buildPortalsMesh()
	{
		Random rnd=new Random();
		float r=rnd.nextFloat();
		float g=rnd.nextFloat();
		float b=rnd.nextFloat();
		MeshBuilder  portalMesh=new MeshBuilder();
		//for (Portal p:this.r.portals)
		{
			com.neoexpert.tr4.level.room.Portal p=this.r.portals[0];
			DrawablePortal p1=Renderer.trPortalToDrawablePortal(this.r.room_info, p);
			MeshBuilder mb=p1.getMesh();
			LinedObject d=new LinedObject(Renderer.fshader,mb.genVertices(),mb.genIndices());
			//DebugDrawables.addDrawable(d);
			Room nextRoom=level.levelData.rooms[p.toRoom];
			if (nextRoom.portals.length > 1)
			{
				p = nextRoom.portals[1];
				DrawablePortal p2 = Renderer.trPortalToDrawablePortal(nextRoom.room_info, p);
				mb = p2.getMesh();
				d = new LinedObject(Renderer.fshader, mb.genVertices(), mb.genIndices());
				//DebugDrawables.addDrawable(d);
				PortalFrustum f=p1.genFrustum(p2);
				mb=f.buildLined();
				d = new LinedObject(Renderer.fshader, mb.genVertices(), mb.genIndices());
				DebugDrawables.clear();
				//DebugDrawables.addDrawable(d);
				
				nextRoom=level.levelData.rooms[p.toRoom];
				if (nextRoom.portals.length > 1)
				{
					p = nextRoom.portals[1];
					DrawablePortal p3 = Renderer.trPortalToDrawablePortal(nextRoom.room_info, p);
					mb = p3.getMesh();
					d = new LinedObject(Renderer.fshader, mb.genVertices(), mb.genIndices());
					DebugDrawables.addDrawable(d);
					com.neoexpert.tr4.Portal.l=true;
					f=f.reduceTo(p3);
					com.neoexpert.tr4.Portal.l=false;
					if(f==null)return;
					mb=f.buildLined();
					d = new LinedObject(Renderer.fshader, mb.genVertices(), mb.genIndices());
					//DebugDrawables.addDrawable(d);
					
				}
			}

//			{
//				//int[] tt=l.texttiles32[t];
//
//				//v0
//				portalMesh.addIndex((short)(portalMesh.fi + 0));
//
//
//				portalMesh.addIndex((short)(portalMesh.fi + 1));
//
//
//				portalMesh.addIndex((short)(portalMesh.fi + 2));
//
//
//				portalMesh.addIndex((short)(portalMesh.fi + 0));
//
//
//				portalMesh.addIndex((short)(portalMesh.fi + 2));
//
//
//				portalMesh.addIndex((short)(portalMesh.fi + 3));
//
//
//				portalMesh.fi += 4;
//
//
//				//XYZ
//				Vec3 pos=new Vec3();
//				pos.x = (room_info.x + p.v0.x) / -1024.0f;
//				pos.y = (p.v0.y) / -1024.0f;
//				pos.z = ((room_info.z + p.v0.z) / 1024.0f);
//				portalMesh.addPosition(pos);
//
//				//color rgba
//
//
//				Vec4 color=new Vec4();
//				color.x = r;//verts[f.v0].getR() / 31.0f;
//
//				color.y = g;// verts[f.v0].getG() / 31.0f;
//
//				color.z = b;// verts[f.v0].getB() / 31.0f;
//
//				color.w = (1.0f);
//				portalMesh.addColor(color);
//
////			//normals
////			roomMesh.add(1.0f;
////			i++;
////			roomMesh.add(0.0f;
////			i++;
////			roomMesh.add(0.0f;
////			i++;
////			//textcoord
//				Vec2 texcoord=new Vec2();
//				portalMesh.addTexCoord(texcoord);
//
//
//				pos = new Vec3();
//				//XYZ
//				pos.x = ((room_info.x + p.v1.x) / -1024.0f);
//
//				pos.y = ((p.v1.y) / -1024.0f);
//
//				pos.z = ((room_info.z + p.v1.z) / 1024.0f);
//				portalMesh.addPosition(pos);
//
//				//color rgba
//
//
//				color = new Vec4();
//				color.x = r; // verts[f.v1].getR()/31.0f;
//
//				color.y = g;// verts[f.v1].getG()/31.0f;
//
//				color.z = b;// verts[f.v1].getB()/31.0f;
//
//				color.w = (1.0f);
//				portalMesh.addColor(color);
//				//normals
////			roomMesh.add(1.0f;
////			i++;
////			roomMesh.add(0.0f;
////			i++;
////			roomMesh.add(0.0f;
////			i++;
////			//textcoord
//				texcoord = new Vec2();
//				portalMesh.addTexCoord(texcoord);
//
//				pos = new Vec3();
//				//XYZ
//				pos.x = ((room_info.x + p.v2.x) / -1024.0f);
//
//				pos.y = ((- p.v2.y) / 1024.0f);
//
//				pos.z = ((room_info.z + p.v2.z) / 1024.0f);
//				portalMesh.addPosition(pos);
//
//				//color rgba
//
//				color = new Vec4();
//				color.x = (r); //verts[f.v2].getR()/31.0f;
//
//				color.y = (g); //verts[f.v2].getG()/31.0f;
//
//				color.z = (b); //verts[f.v2].getB()/31.0f;
//
//				color.w = (1.0f);
//				portalMesh.addColor(color);
//				//normals
////			roomMesh.add(1.0f;
////			i++;
////			roomMesh.add(0.0f;
////			i++;
////			roomMesh.add(0.0f;
////			i++;
//
//				//textcoord
//				texcoord = new Vec2();
//				portalMesh.addTexCoord(texcoord);
//
//				//XYZ
//				pos = new Vec3();
//				pos.x = ((room_info.x + p.v3.x) / -1024.0f);
//
//				pos.y = ((-p.v3.y) / 1024.0f);
//
//				pos.z = ((room_info.z + p.v3.z) / 1024.0f);
//				portalMesh.addPosition(pos);
//
//				//color rgba
//
//
//				color = new Vec4();
//				color.x = (r); // verts[f.v3].getR()/31.0f;
//
//				color.y = (g);// verts[f.v3].getG()/31.0f;
//
//				color.z = (b);// verts[f.v3].getB()/31.0f;
//
//
//				color.w = (1.0f);
//				portalMesh.addColor(color);
//
//				//normals
////			roomMesh.add(1.0f;
////			i++;
////			roomMesh.add(0.0f;
////			i++;
////			roomMesh.add(0.0f;
////			i++;
////			//textcoord
//				texcoord = new Vec2();
//				portalMesh.addTexCoord(texcoord);
//			}
			//portalMesh.calculateNormals();
			//this.portalMesh = new DoubleSidedMesh(shader, portalMesh.genVertices(), portalMesh.genIndices());		
			//this.portalMesh.setAdditiveBlending(true);

		}
	}
	
	
	public boolean hasVertices(){
		return true;
	}

	

	float[] mvpMatrix=new float[16];

	@Override
	public void draw(float[] mvpMatrix)
	{
		GLES30.glUniformMatrix4fv(shader.mMMatrixHandle, 1, false, mModelMatrix, 0);
		super.draw(mvpMatrix);
		
		for(GLDrawable d:children)
			d.draw(mvpMatrix);
		for(GLDrawable d:transparent_children)
			d.draw(mvpMatrix);
	}
	public void drawPortals(float[] mvpMatrix){
		for(DrawablePortal p:portals)
			p.draw(mvpMatrix);
	}

	
}
