package com.neoexpert.tr4;

import com.neoexpert.math.*;
import com.neoexpert.mesh.*;
import com.neoexpert.rendering.*;


public class PortalFrustum{
    //nbl
    public Vec3 nbl;
    //ntl
    public Vec3 ntl;
    //ntr
    public Vec3 ntr;
    //nbr
    public Vec3 nbr;

    //fbl
    public Vec3 fbl;
    //ftl
    public Vec3 ftl;
    //ftr
    public Vec3 ftr;
    //fbr
    public Vec3 fbr;

    public Vec3 v0 =new Vec3(0,0,0);
    public Vec3 v1 =new Vec3(0,0,0);
    public Plane nearPlane=new Plane();
    public Plane leftPlane=new Plane();
    public Plane rightPlane=new Plane();
    public Plane topPlane=new Plane();
    public Plane bottomPlane=new Plane();
		public Portal portal;

    public PortalFrustum(Portal portal) {
			this.portal=portal;
		}

    public boolean contains(Vec3 tp) {
        float db = topPlane.distance(tp);
        float dt = bottomPlane.distance(tp);
        float dl = rightPlane.distance(tp);
        float dr = leftPlane.distance(tp);
        return topPlane.facing(tp)&bottomPlane.facing(tp)&leftPlane.facing(tp)&rightPlane.facing(tp);
    }

    public boolean intersects(Vec3 tp,float radius) {
        radius=-radius;
        float td = topPlane.distance(tp);
        float bd = bottomPlane.distance(tp);
        float ld = leftPlane.distance(tp);
        float rd = rightPlane.distance(tp);
		if(Portal.l){
			MainActivity.logger.log(td+">"+radius);
			MainActivity.logger.log(bd+">"+radius);
			MainActivity.logger.log(ld+">"+radius);
			MainActivity.logger.log(rd+">"+radius);
		}
        return topPlane.distance(tp)>radius&bottomPlane.distance(tp)>radius&leftPlane.distance(tp)<radius&rightPlane.distance(tp)<radius;
    }

			 
	public PortalFrustum reduceTo(Portal p){
		if(!portal.isVisibleFrom(p))
			return null;
		if (!intersects(p.center, p.radius)){
			if(Portal.l){
				intersects(p.center, p.radius);
				MainActivity.logger.log("no intersection");
			}
			return null;
		}
		PortalFrustum f = new PortalFrustum(portal);
		f.nbl = nbl;
		f.nbr = nbr;
		f.ntl = ntl;
		f.ntr = ntr;
		{
			Vec3 ftl1 = topPlane.intersect(f.ntl, p.l02);
			//f.ftl = nftl;
			if (ftl1.subtract(p.v0).dot(p.l02.dir) < 0)
				f.ftl = ftl1;
			else f.ftl = p.v0;

			Vec3 ftr1 = topPlane.intersect(f.ntl, p.l13);
			//f.ftr = nftr;
			if (ftr1.subtract(p.v1).dot(p.l13.dir) < 0)
				f.ftr = ftr1;
			else f.ftr = p.v1;


			Vec3 fbl1 = bottomPlane.intersect(f.nbl, p.l02);
			if (fbl1.subtract(p.v2).dot(p.l02.dir) > 0)
				f.fbl = fbl1;
			else f.fbl = p.v2;

			Vec3 fbr1 = bottomPlane.intersect(f.nbl, p.l13);
			//f.fbr = nfbr;
			if (fbr1.subtract(p.v3).dot(p.l13.dir) > 0)
				f.fbr = fbr1;
			else f.fbr = p.v3;
		}

		{
			Line l01 = new Line(f.ftl, f.ftl.subtract(f.ftr));
			Line l23 = new Line(f.fbl, f.fbl.subtract(f.fbr));

			Vec3 ftr1 = rightPlane.intersect(f.ntr, l01);
			//f.ftl = nftl;
			if (ftr1.subtract(f.ftr).dot(l01.dir) > 0)
				f.ftr = ftr1;
			else f.ftr = f.ftr;

			Vec3 fbr1 = rightPlane.intersect(f.ntr, l23);
			//f.ftr = nftr;
			if (fbr1.subtract(f.fbr).dot(l23.dir) > 0)
				f.fbr = fbr1;
			else f.fbr = f.fbr;


			Vec3 fbl1 = leftPlane.intersect(f.nbl, l23);
			if (fbl1.subtract(f.fbl).dot(l23.dir) < 0)
				f.fbl = fbl1;
			else f.fbl = f.fbl;

			Vec3 ftl1 = leftPlane.intersect(f.nbl, l01);
			//f.fbr = nfbr;
			if (ftl1.subtract(f.ftl).dot(l01.dir) < 0)
				f.ftl = ftl1;
			else f.ftl = f.ftl;
		}
		f.rebuildPlanes();
		return f;
	}

	private void rebuildPlanes()
	{
		Vec3 cv0=ftl.subtract(ntl);
		Vec3 cv1=ftr.subtract(ntr);
		Vec3 cv2=fbl.subtract(nbl);
		Vec3 cv3=fbr.subtract(nbr);
		topPlane = new Plane(ntl, cv0.cross(cv1).normalize());
		bottomPlane = new Plane(nbl, cv3.cross(cv2).normalize());
		rightPlane = new Plane(ntl, cv2.cross(cv0).normalize());
		leftPlane = new Plane(ntr, cv1.cross(cv3).normalize());
	}

    public MeshBuilder buildLined(){
	    MeshBuilder mb=new MeshBuilder();
	    createLinesIndexes(mb);
		v0=nbr;
		v1=nbr.add(rightPlane.getNormal().multiply(100f));
	    build(mb);
	    return mb;
    
    }
    public MeshBuilder buildFaced(){
	    MeshBuilder mb=new MeshBuilder();
	    createTrianglesIndexes(mb);
	    build(mb);
	    return mb;
    
    }
    private void build(MeshBuilder mb){
        //mb.addIndex((short) 8);
        //mb.addIndex((short) 9);


        //mb.addIndex((short) 1);
        //mb.addIndex((short) 3);

        //0
        mb.addVertex(nbl.x, nbl.y, nbl.z,1,0,0,1,1,1);
        //1
        mb.addVertex(ntl.x, ntl.y, ntl.z,0,1,0,1,1,1);
        //2
        mb.addVertex(ntr.x, ntr.y, ntr.z,0,0,1,1,1,1);
        //3
        mb.addVertex(nbr.x, nbr.y, nbr.z,0,1,1,1,1,1);

        //4
        mb.addVertex(fbl.x, fbl.y, fbl.z,0,1,1,1,1,1);
        //5
        mb.addVertex(ftl.x, ftl.y, ftl.z,0,0,1,1,1,1);
        //6
        mb.addVertex(ftr.x, ftr.y, ftr.z,0,1,0,1,1,1);
        //7
        mb.addVertex(fbr.x, fbr.y, fbr.z,1,0,0,1,1,1);

        mb.addVertex(v0.x, v0.y, v0.z,1,0,0,1,1,1);
        mb.addVertex(v1.x, v1.y, v1.z,0,0,1,1,1,1);

    }

    private void createTrianglesIndexes(MeshBuilder mb) {
        mb.addIndex((short) 0);
        mb.addIndex((short) 1);
        mb.addIndex((short) 2);
        mb.addIndex((short) 0);
        mb.addIndex((short) 2);
        mb.addIndex((short) 3);


        mb.addIndex((short) 0);
        mb.addIndex((short) 1);
        mb.addIndex((short) 5);
        mb.addIndex((short) 0);
        mb.addIndex((short) 5);
        mb.addIndex((short) 4);

        mb.addIndex((short) 3);
        mb.addIndex((short) 2);
        mb.addIndex((short) 6);
        mb.addIndex((short) 3);
        mb.addIndex((short) 6);
        mb.addIndex((short) 7);

        mb.addIndex((short) 1);
        mb.addIndex((short) 2);
        mb.addIndex((short) 5);
        mb.addIndex((short) 2);
        mb.addIndex((short) 5);
        mb.addIndex((short) 6);

        mb.addIndex((short) 0);
        mb.addIndex((short) 3);
        mb.addIndex((short) 4);
        mb.addIndex((short) 3);
        mb.addIndex((short) 7);
        mb.addIndex((short) 4);

        mb.addIndex((short) 4);
        mb.addIndex((short) 5);
        mb.addIndex((short) 6);
        mb.addIndex((short) 4);
        mb.addIndex((short) 6);
        mb.addIndex((short) 7);
    }

    private void createLinesIndexes(MeshBuilder mb) {
        mb.addIndex((short) 0);
        mb.addIndex((short) 1);
        mb.addIndex((short) 1);
        mb.addIndex((short) 2);
        mb.addIndex((short) 2);
        mb.addIndex((short) 3);
        mb.addIndex((short) 3);
        mb.addIndex((short) 0);

        mb.addIndex((short) 4);
        mb.addIndex((short) 5);
        mb.addIndex((short) 5);
        mb.addIndex((short) 6);
        mb.addIndex((short) 6);
        mb.addIndex((short) 7);
        mb.addIndex((short) 7);
        mb.addIndex((short) 4);


        mb.addIndex((short) 0);
        mb.addIndex((short) 4);

        mb.addIndex((short) 1);
        mb.addIndex((short) 5);

        mb.addIndex((short) 2);
        mb.addIndex((short) 6);

        mb.addIndex((short) 3);
        mb.addIndex((short) 7);

        mb.addIndex((short) 8);
        mb.addIndex((short) 9);
    }


}
