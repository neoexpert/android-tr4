package com.neoexpert.tr4;

import android.app.*;
import android.content.*;
import android.content.pm.*;
import android.net.*;
import android.opengl.*;
import android.os.*;
import android.text.method.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import com.neoexpert.*;
import com.neoexpert.tr4.level.*;
import java.io.*;
import com.neoexpert.tr4.level.room.*;
import com.neoexpert.tr4.level.animation.*;

public class MainActivity extends Activity 
implements 
ALogger,
OnTouchListener,
Renderer.OnSurfaceCreatedCallback
{
	@Override
	public void onSurdaceCreated()
	{	
		if(level!=null)return;
		try
		{
			InputStream is = Util.getLevelStream(this, R.raw.title);
			loadLevel(is);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
		
		
	}

	private void loadLevel(final InputStream is)
	{
		
		lrenderer = new LevelRenderer(this, this, renderer.getRoomShader());
		erenderer=new EntityRenderer(this,this,renderer.getRoomShader());
		renderer.setRenderer(lrenderer);
		
		final Activity ctx=this;
	
			new Thread(new Runnable(){

					@Override
					public void run()
					{
						Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(ctx));
						try
						{
							level = Level.read(is);					
							com.neoexpert.tr4.level.room.Entity lara = level.levelData.lara;
							renderer.setLevel(level);
							lrenderer.setLevel(level);
							erenderer.setLevel(level);
							if (entityMode)
								renderer.setRenderer(erenderer);
							else
								renderer.setRenderer(lrenderer);
							renderer.setRotX(rotx);
							renderer.setRotY(-roty);
							ctx.runOnUiThread(new Runnable(){
									@Override
									public void run()
									{
										findViewById(R.id.loading).setVisibility(View.GONE);
									}


								});
							//findViewById(R.id.getlevels).setVisibility(View.GONE);
						}
						catch (IOException e)
						{
							throw new RuntimeException(e);
						}
					}


				}).start();
		
		
		
	}
	
	float dx;
	float dy;
	float roty=-(float) Math.PI;
	float rotx=(float) Math.PI/2;

	private TextView entityId;

	private Level level;
	
	@Override
	public boolean onTouch(View v, MotionEvent e)
	{
		
		switch (v.getId())
		{
			case 0:
				switch (e.getActionMasked())
				{
					case MotionEvent.ACTION_DOWN:
					
						dx = e.getX();
						dy = e.getY();
						return true;
					case MotionEvent.ACTION_UP:
					case MotionEvent.ACTION_MOVE:
						//log(roty+"");
						roty += (e.getX() - dx)/100;
						rotx += (e.getY() - dy)/100;
						dx = e.getX();
						dy = e.getY();
						renderer.setRotY(-roty);
						renderer.setRotX(rotx);
						return true;
				}
				return true;
			case R.id.w:
				switch (e.getActionMasked())
				{
					case MotionEvent.ACTION_POINTER_DOWN:
					case MotionEvent.ACTION_POINTER_UP:
						return false;
					case MotionEvent.ACTION_UP:
					
						renderer.w(false);
						return true;
					case MotionEvent.ACTION_DOWN:
						renderer.w(true);
						return true;
				}
				break;
			case R.id.s:
				switch (e.getActionMasked())
				{
					case MotionEvent.ACTION_UP:
						renderer.s(false);
						return true;
					case MotionEvent.ACTION_DOWN:
						renderer.s(true);
						return true;
				}
				break;
			case R.id.up:
				switch (e.getActionMasked())
				{
					case MotionEvent.ACTION_UP:
						renderer.up(false);
						return true;
					case MotionEvent.ACTION_DOWN:
						renderer.up(true);
						return true;
				}
				break;
			case R.id.down:
				switch (e.getActionMasked())
				{
					case MotionEvent.ACTION_UP:
						renderer.down(false);
						return true;
					case MotionEvent.ACTION_DOWN:
						renderer.down(true);
						return true;
				}
				break;
			
			case R.id.fl:
				log("fl");
				break;
			default:
				log(v.getId()+"");
				break;
			
		}
		return false;
	}
	
	@Override
	public void seText(final String s)
	{
		runOnUiThread(new Runnable(){

				@Override
				public void run()
				{
					log.setText(s);
				}
			});
	}
	public static ALogger logger;
	@Override
	public void log(final String s)
	{
		runOnUiThread(new Runnable(){

				@Override
				public void run()
				{
					log.setText(s+"\n"+ log.getText());
				}
			});
		
	}
	


	/** Hold a reference to our GLSurfaceView */
	private GLSurfaceView mGLSurfaceView;

	private LevelRenderer lrenderer;
	private EntityRenderer erenderer;
	private Renderer renderer;
	private TextView log;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		logger=this;
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		mGLSurfaceView = new GLSurfaceView(this);
		
		// Check if the system supports OpenGL ES 2.0.
		final ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		final ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();
		final boolean supportsEs2 = configurationInfo.reqGlEsVersion >= 0x20000;

		if (supportsEs2) 
		{
			// Request an OpenGL ES 2.0 compatible context.
			mGLSurfaceView.setEGLContextClientVersion(3);
			renderer=new Renderer(this,this);
			// Set the renderer to our demo renderer, defined below.
			
			
			renderer.setOnSurfaceCreatedCallBack(this);
			//roty=-20;
			
			mGLSurfaceView.setRenderer(renderer);
		} 
		else 
		{
			// This is where you could create an OpenGL ES 1.x compatible
			// renderer if you wanted to support both ES 1 and ES 2.
			return;
		}
		LinearLayout ll=findViewById(R.id.ll);
		ll.addView(mGLSurfaceView);
		mGLSurfaceView.setOnTouchListener(this);
		mGLSurfaceView.setId(0);
		
		//View v=inflateControls();
		//FrameLayout root=(FrameLayout)findViewById(R.id.root);
		//root.addView(v,1);
		//root.setOnTouchListener(this);
		
		
		log=findViewById(R.id.log);
		log.setMovementMethod(new ScrollingMovementMethod());
		
		
		findViewById(R.id.w).setOnTouchListener(this);
		findViewById(R.id.s).setOnTouchListener(this);
		findViewById(R.id.down).setOnTouchListener(this);
		findViewById(R.id.up).setOnTouchListener(this);
		entityId=(TextView)findViewById(R.id.enttity_id);
		((View)findViewById(R.id.log_button)).setVisibility(View.VISIBLE);
		
	}

	@Override
	protected void onResume() 
	{
		// The activity must call the GL surface view's onResume() on activity onResume().
		super.onResume();
		mGLSurfaceView.onResume();
		
		
	}

	@Override
	protected void onPause() 
	{
		// The activity must call the GL surface view's onPause() on activity onPause().
		super.onPause();
		mGLSurfaceView.onPause();
	}	
	public void onClick(View v){
		Intent intent = new Intent()
			.setType("*/*")
			.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select a file"), 123);
		
	}
	
	public void showLog(View v){
		if(log.getVisibility()==View.VISIBLE)
			log.setVisibility(View.GONE);
		else
			log.setVisibility(View.VISIBLE);
		
	}
	
	public void toggleFlybyCamera(View v){
		CheckBox cb=(CheckBox) v;
		renderer.toggleFlybyCamera(cb.isChecked());
		if(cb.isChecked()){
			findViewById(R.id.main_controls).setVisibility(View.INVISIBLE);
			findViewById(R.id.cross).setVisibility(View.INVISIBLE);
			findViewById(R.id.updown).setVisibility(View.INVISIBLE);
			findViewById(R.id.flyByCameraText).setVisibility(View.INVISIBLE);
		}
		else{
			findViewById(R.id.main_controls).setVisibility(View.VISIBLE);
			findViewById(R.id.cross).setVisibility(View.VISIBLE);
			findViewById(R.id.updown).setVisibility(View.VISIBLE);
			findViewById(R.id.flyByCameraText).setVisibility(View.VISIBLE);
		}
	}
	boolean entityMode=false;
	
	public void switchMode(View v){
		entityMode=!entityMode;
		if(entityMode){
			rotx=erenderer.rotx;
			roty=erenderer.roty;
			renderer.setRenderer(erenderer);
			((Button)v).setText("Level");
			findViewById(R.id.entity_controls).setVisibility(View.VISIBLE);
			findViewById(R.id.level_controls).setVisibility(View.GONE);
			renderer.toggleFlybyCamera(false);
		}
		else{
			rotx=lrenderer.rotx;
			roty=lrenderer.roty;
			renderer.setRenderer(lrenderer);
			((Button)v).setText("Entities");
			//findViewById(R.id.entity_controls).setVisibility(View.GONE);
			findViewById(R.id.level_controls).setVisibility(View.VISIBLE);
		}
		
	}
	
	int entityIndex=3;
	public void nextEntity(View v){
		if(level.levelData.items.length>entityIndex)
			entityIndex++;
		renderer.setEntity(entityIndex);
		
		entityId.setText(""+entityIndex);
		
	}
	
	public void prevEntity(View v){
		if(entityIndex>0)
			entityIndex--;
		renderer.setEntity(entityIndex);
		entityId.setText(""+entityIndex);
		
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==123 && resultCode==RESULT_OK) {
            Uri selectedfile = data.getData(); //The uri with the location of the file
			try
			{
				
				InputStream is=getContentResolver().openInputStream(selectedfile);
				findViewById(R.id.loading).setVisibility(View.VISIBLE);
				findViewById(R.id.getlevels).setVisibility(View.GONE);
				loadLevel(is);
				

			}
			catch (IOException e)
			{
				throw new RuntimeException(e);
			}

        }
    }
	public void getLevels(View v){
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/andrewsyc/Tomb-Raider-1-2-3-4-5-Map-viewer-and-levels/tree/master/Tomb-Raider-4"));
		startActivity(browserIntent);
	}
	
}
