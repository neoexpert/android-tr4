package com.neoexpert.tr4;
import java.util.*;
import com.neoexpert.*;
import com.neoexpert.rendering.Camera;
import com.neoexpert.tr4.level.room.*;

public abstract class RendererInterface
{
	Camera cam=new Camera();
	public float roty=-(float) Math.PI;
	public float rotx=(float) Math.PI/2;
	
	protected ALogger logger;
	protected ArrayList<GLDrawable> drawables=new ArrayList<>();
	protected ArrayList<GLDrawable> tramsparent_drawables=new ArrayList<>();
	
	public RendererInterface(ALogger logger){
		this.logger=logger;
		cam.setUp(0,1,0);
	}

	public abstract TRRoom getRoom(int roomBelow)
	
	public ArrayList<GLDrawable> getTransparentDrawables()
	{
		return tramsparent_drawables;
	}


	public ArrayList<GLDrawable> getDrawables()
	{
		return drawables;
	}
	public abstract Entity setEntity(int entityIndex);
}
