package com.neoexpert.tr4;

import com.neoexpert.math.*;
import com.neoexpert.mesh.*;
import com.neoexpert.rendering.*;
import com.neoexpert.tr4.shader.*;
import com.neoexpert.tr4.mesh.*;
import java.util.*;

public class DebugDrawables {
	private static List<Object3D> drawables=new ArrayList<>();

	public static void clear()
	{
		synchronized(drawables){
			drawables.clear();
		}
	}

	public static void addDrawable(Object3D d)
	{
		synchronized (drawables)
		{
			drawables.add(d);
		}
	}
	public static void draw(float[] mvpMatrix)
	{
		synchronized (drawables)
		{
			for (Object3D o:drawables)
			{
				o.draw(mvpMatrix);
			}
		}
	}
    public static void portalTest2(NeoShader shader) {
        PortalFrustum f1;
        PortalFrustum f2;
        PortalFrustum f3;
        {
            Portal p1=new Portal(
                    new Vec3(100,0,-1500),
                    new Vec3(0,0,-1500),
                    new Vec3(100,-100,-1500),
                    new Vec3(0,-100,-1500));
            p1.setColor(1,0,0);
            MeshBuilder mb = p1.getMesh();
            LinedObject p1m=new LinedObject(shader,mb.genVertices(),mb.genIndices());
            p1m.setShader(shader);
			
            addDrawable(p1m);
			
			

            Portal p3=new Portal(
                    new Vec3(100,-250,-400),
                    new Vec3(0,-250,-400),
                    new Vec3(100,-250,-500),
                    new Vec3(0,-250,-500));
            p3.setColor(0,1,0);
            mb = p3.getMesh();
            LinedObject p3m=new LinedObject(shader,mb.genVertices(),mb.genIndices());
            p3m.setShader(shader);
			addDrawable(p3m);

            //Portal p2=new Portal(new Vec3(0,100,-500),new Vec3(100,100,-500),new Vec3(0,0,-500),new Vec3(100,0,-500));
            Portal p2=new Portal(
                    new Vec3(100,-100,-1000),
                    new Vec3(0,-100,-1000),
                    new Vec3(100,-200,-1000),
                    new Vec3(0,-200,-1000));
            p2.setColor(0,0,1);
            mb = p2.getMesh();
            LinedObject p2m=new LinedObject(shader,mb.genVertices(),mb.genIndices());
            p2m.setShader(shader);
	
			addDrawable(p2m);
            	
			

            f1=p1.genFrustum(p2);
            mb=f1.buildLined();
            LinedObject frustumMesh1=new LinedObject(shader,mb.genVertices(),mb.genIndices());
            frustumMesh1.setShader(shader);
            //renderer.addDrawable(frustumMesh);

            f3 = f1.reduceTo(p3);
			if(f3==null)
				return;
	         mb=f3.buildLined();
            LinedObject frustumMesh3=new LinedObject(shader,mb.genVertices(),mb.genIndices());
            frustumMesh3.setShader(shader);
			
            f2=p2.genFrustum(p3);


			
            mb=f2.buildLined();
            LinedObject frustumMesh2=new LinedObject(shader,mb.genVertices(),mb.genIndices());
            frustumMesh2.setShader(shader);
            //renderer.addDrawable(frustumMesh2);

            mb= PlaneMesh.genPlaneMeshXY(f3.topPlane,1,1,0);
            LinedObject planeMesh=new LinedObject(shader,mb.genVertices(),mb.genIndices());
            planeMesh.setShader(shader);
            //renderer.addDrawable(planeMesh);

            mb=PlaneMesh.genPlaneMeshXZ(f3.bottomPlane,0,1,1);
            planeMesh=new LinedObject(shader,mb.genVertices(),mb.genIndices());
            planeMesh.setShader(shader);
            //addDrawable(planeMesh);
			//addDrawable(frustumMesh1);
			//addDrawable(frustumMesh2);
			addDrawable(frustumMesh3);
		}
		
        return;
    }
}
