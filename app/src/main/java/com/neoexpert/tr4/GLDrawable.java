package com.neoexpert.tr4;

import android.opengl.*;

public abstract class GLDrawable
{
	public float[] mModelMatrix = new float[16];

	private boolean additiveBlending = false;
	public GLDrawable(){
		Matrix.setIdentityM(mModelMatrix, 0);
		
	}
	public void setAdditiveBlending(boolean a)
	{
		this.additiveBlending=a;
	}
	public boolean hasAddidiveBlending()
	{
		return additiveBlending;
	}
	
	public abstract void draw(float[] mvpMatrix) 
		
}
