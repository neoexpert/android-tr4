package com.neoexpert.tr4;

import android.opengl.*;
import com.neoexpert.tr4.shader.*;
import java.nio.*;
import java.util.*;

public class Object3D extends GLDrawable
{
	public static final int POSITION_COORDS=3;
	public static final int NORMAL_COORDS=3;
	public static final int COLOR_COORDS=4;
	public static final int TEX_COORDS=2;
	public static final int COORDS_PER_VERTEX=POSITION_COORDS+NORMAL_COORDS+COLOR_COORDS+TEX_COORDS;
	public static final int VERTEX_STRIDE=COORDS_PER_VERTEX*4;
	protected NeoShader shader;

	public Object3D(){}
	public Object3D(NeoShader shader)
	{
		this.shader = shader;
	}
	public Object3D(NeoShader shader,FloatBuffer vertices, ShortBuffer indices)
	{
		this.shader = shader;
		
		setVertices(vertices);

		setIndices(indices);
		
	}

	public void setShader(NeoShader shader)
	{
		this.shader = shader;
	}

	public NeoShader getShader()
	{
		return shader;
	}
	public void setVertices(FloatBuffer fb){
		mVertices = fb;
		
	}
	public void setIndices(ShortBuffer sb){
		drawOrder = sb;
		drawOrder.position(0);

	}
	protected FloatBuffer mVertices;

	protected ShortBuffer drawOrder;
	protected float lx;

	protected float ly;

	protected float lz;
	@Override
	public void draw(float[] mvpMatrix)
	{
		if(drawOrder.capacity()<1)
			return;
		// Use the program object
		
		
		// get handle to fragment shader's vColor member
		// mColorHandle = GLES30.glGetUniformLocation(mProgramObject, "vColor");


		// Apply the projection and view transformation
		GLES30.glUniformMatrix4fv(shader.mMVPMatrixHandle, 1, false, mvpMatrix, 0);
		Renderer.checkGlError();



		mVertices.position(0);  //just in case.  We did it already though.

		//add all the points to the space, so they can be correct by the transformations.
		//would need to do this even if there were no transformations actually.
		GLES30.glVertexAttribPointer(shader.mPositionHandle, POSITION_COORDS, GLES30.GL_FLOAT,
									 true, VERTEX_STRIDE, mVertices);

		Renderer.checkGlError();
		GLES30.glEnableVertexAttribArray(shader.mPositionHandle);

		Renderer.checkGlError();
		
		mVertices.position(3);  //just in case.  We did it already though.
		GLES30.glVertexAttribPointer(shader.mNormalHandle, NORMAL_COORDS, GLES30.GL_FLOAT,
									 true, VERTEX_STRIDE, mVertices);

		Renderer.checkGlError();
		GLES30.glEnableVertexAttribArray(shader.mNormalHandle);

		Renderer.checkGlError();
		mVertices.position(6);  //just in case.  We did it already though.
		
		//add all the points to the space, so they can be correct by the transformations.
		//would need to do this even if there were no transformations actually.
		GLES30.glVertexAttribPointer(shader.mColorHandle, COLOR_COORDS, GLES30.GL_FLOAT,
									 true, VERTEX_STRIDE, mVertices);

		GLES30.glEnableVertexAttribArray(shader.mColorHandle);
		//Renderer.checkGlError();
		mVertices.position(10);  //just in case.  We did it already though.

		//add all the points to the space, so they can be correct by the transformations.
		//would need to do this even if there were no transformations actually.
		GLES30.glVertexAttribPointer(shader.mTexCoordHandle, TEX_COORDS, GLES30.GL_FLOAT,
									 true, VERTEX_STRIDE, mVertices);
//									 
		//Renderer.checkGlError();					 
		GLES30.glEnableVertexAttribArray(shader.mTexCoordHandle);
//						 


//									 
		Renderer.checkGlError();					 
		
		draw();
	}
	public void draw(){
		GLES30.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.capacity(),GLES20.GL_UNSIGNED_SHORT,drawOrder);
	}
}
