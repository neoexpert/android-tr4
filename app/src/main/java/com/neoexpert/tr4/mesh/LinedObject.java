package com.neoexpert.tr4.mesh;
import android.opengl.*;
import com.neoexpert.tr4.*;
import com.neoexpert.tr4.shader.*;
import java.nio.*;

public class LinedObject extends Object3D
{
	public LinedObject(){}
	public LinedObject(NeoShader shader,FloatBuffer vertices, ShortBuffer indices)
	{
		super(shader,vertices,indices);
	}
	@Override
	public void draw(float[] mvpMatrix)
	{
		if(drawOrder.capacity()<1)
			return;
		GLES30.glUseProgram(shader.mProgramObject);
		// Use the program object


		// get handle to fragment shader's vColor member
		// mColorHandle = GLES30.glGetUniformLocation(mProgramObject, "vColor");


		// Apply the projection and view transformation
		GLES30.glUniformMatrix4fv(shader.mMVPMatrixHandle, 1, false, mvpMatrix, 0);
		Renderer.checkGlError();



		mVertices.position(0);  //just in case.  We did it already though.

		//add all the points to the space, so they can be correct by the transformations.
		//would need to do this even if there were no transformations actually.
		GLES30.glVertexAttribPointer(shader.mPositionHandle, POSITION_COORDS, GLES30.GL_FLOAT,
									 true, VERTEX_STRIDE, mVertices);

		Renderer.checkGlError();
		GLES30.glEnableVertexAttribArray(shader.mPositionHandle);

		Renderer.checkGlError();

		Renderer.checkGlError();
		mVertices.position(6);  //just in case.  We did it already though.

		//add all the points to the space, so they can be correct by the transformations.
		//would need to do this even if there were no transformations actually.
		GLES30.glVertexAttribPointer(shader.mColorHandle, COLOR_COORDS, GLES30.GL_FLOAT,
									 true, VERTEX_STRIDE, mVertices);

		GLES30.glEnableVertexAttribArray(shader.mColorHandle);
		//Renderer.checkGlError();
		mVertices.position(8);  //just in case.  We did it already though.

		//add all the points to the space, so they can be correct by the transformations.
		//would need to do this even if there were no transformations actually.
		GLES30.glVertexAttribPointer(shader.mTexCoordHandle, TEX_COORDS, GLES30.GL_FLOAT,
									 true, VERTEX_STRIDE, mVertices);
//									 
		//Renderer.checkGlError();					 
		GLES30.glEnableVertexAttribArray(shader.mTexCoordHandle);
//						 


//									 
		Renderer.checkGlError();		
		GLES31.glLineWidth(10);
		GLES31.glDisable(GLES31.GL_CULL_FACE);
		GLES30.glDrawElements(GLES20.GL_LINES, drawOrder.capacity(),GLES20.GL_UNSIGNED_SHORT,drawOrder);
		GLES31.glEnable(GLES31.GL_CULL_FACE);
	}
}
