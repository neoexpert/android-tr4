package com.neoexpert.tr4.mesh;



import android.content.*;
import android.opengl.*;
import com.neoexpert.*;
import com.neoexpert.math.*;
import com.neoexpert.mesh.*;
import com.neoexpert.tr4.*;
import com.neoexpert.tr4.level.*;
import com.neoexpert.tr4.level.mesh.*;
import com.neoexpert.tr4.level.room.*;
import com.neoexpert.tr4.shader.*;
import java.util.*;

import android.opengl.Matrix;

public class TRMesh extends Object3D{

	private Context context;

	//private NeoShader shader;

	private ALogger logger;

	private boolean hasAdditiveBlending;

	public int roty;

	public int rotz;

	public int rotx;

	private boolean hasVertices;

	private float mred;

	private float mgreen;

	private float mblue;

	private boolean canMark;

	

    public TRMesh(Mesh m,Level l,Context ctx,ALogger logger) {
		super();
		canMark=shader instanceof MarkingShader;
		this.logger=logger;
		this.context=ctx;
		//this.shader=shader;
		
		
		Vertex center=m.centre;

		//room_info.yBottom=200;
		Vertex[] verts=m.vertices;
		MeshFace4[] rects=m.textRects;
		MeshFace3[] triangles=m.textTriangles;
		//rects=new Face4[1];

		int rectscount=1;
		int tricount=0;
		rectscount=rects.length;
		tricount=triangles.length;
		int meshSize=(rectscount*4+3*tricount)*(3+4+2);
		hasVertices=meshSize>0;
		MeshBuilder mb=new MeshBuilder();
		//float[] roomMesh=new float[meshSize];
		//short[] roomIndices=new short[tricount*3+ rectscount*6];
	
		//short fi=0;

		//int a=l.textures32.length;


		int twidth=(int)Math.sqrt(l.textures32.length)+1;
		for(int j=0;j< rectscount;j++){
			MeshFace4 f=rects[j];
			if(f.hasAlphaBlending())
				hasAdditiveBlending=true;
			int t=0;
			ObjectTexture ot=null;
			if (!f.isColored)
			{
				ot=l.levelData.objectTextures[f.getTexture()];
				t = ot.getTile();
			}
			int tx=t%twidth;
			int ty=t/twidth;

			//v0
			mb.addIndex((short)(mb.fi+0));

			mb.addIndex((short)(mb.fi+1));

			mb.addIndex((short)(mb.fi+2));

			mb.addIndex((short)(mb.fi+0));

			mb.addIndex((short)(mb.fi+2));

			mb.addIndex((short)(mb.fi+3));

			mb.fi+=4;


			
			Vec3 pos=new Vec3();
			pos.x=((verts[f.v0].x) / -1024.0f);

			pos.y=((verts[f.v0].y)/-1024.0f);

			pos.z=((verts[f.v0].z)/1024.0f);

			mb.addPosition(pos);
			
			//color
			Vec4 color=new Vec4();
			color.x=(1.0f);//verts[f.v0].getR()/32.0f;

			
			color.y=(1.0f);//verts[f.v0].getG()/32.0f;

			
			color.z=(1.0f);//verts[f.v0].getB()/32.0f;

			
			color.w=(1.0f);
			mb.addColor(color);
			
//			//normals
//			mb.add(1.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			//textcoord
			//f.t=3;
			Vec2 texcoord=new Vec2();
			if (!f.isColored)
			{
				texcoord.x=((tx * 256 + (ot.v0.xPixel)) / (twidth * 256.0f));
				texcoord.y=((ty*256+(ot.v0.yPixel))/(twidth*256.0f));
			}
			else{
				texcoord.x=(0);
				texcoord.y=(0);
			}
			mb.addTexCoord(texcoord);

			pos=new Vec3();
			pos.x=((verts[f.v1].x)/-1024.0f);
			
			pos.y=((verts[f.v1].y)/-1024.0f);
			
			pos.z=((verts[f.v1].z)/1024.0f);
			mb.addPosition(pos);
			
			//color
			color=new Vec4();
			color.x=(1.0f);//verts[f.v1].getR()/32.0f;
			
			color.y=(1.0f);//verts[f.v1].getG()/32.0f;
			
			color.z=(1.0f);//verts[f.v1].getB()/32.0f;
			
			color.w=(1.0f);
			mb.addColor(color);
			
			//normals
//			mb.add(1.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			//textcoord
			texcoord=new Vec2();
			if (!f.isColored)
			{
				texcoord.x=((tx * 256 + (ot.v1.xPixel)) / (twidth * 256.0f));
				

				texcoord.y=((ty * 256 + (ot.v1.yPixel)) / (twidth * 256.0f));
				
			}
			else{
				texcoord.x=(0);
				texcoord.y=(0);
			}
			mb.addTexCoord(texcoord);

			pos=new Vec3();
			pos.x=((verts[f.v2].x)/-1024.0f);
			
			pos.y=((verts[f.v2].y)/-1024.0f);
			
			pos.z=((verts[f.v2].z)/1024.0f);
			mb.addPosition(pos);
			
			//color
			color=new Vec4();
			color.x=(1.0f);//verts[f.v2].getR()/32.0f;
			
			color.y=(1.0f);//verts[f.v2].getG()/32.0f;
			
			color.z=(1.0f);//verts[f.v2].getB()/32.0f;
			
			color.w=(1.0f);
			mb.addColor(color);
			//normals
//			mb.add(1.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			//textcoord
			texcoord=new Vec2();
			if(!f.isColored){
				texcoord.x=((tx*256+(ot.v2.xPixel))/(twidth*256.0f));
				texcoord.y=((ty*256+(ot.v2.yPixel))/(twidth*256.0f));
			}
			else{
				texcoord.x=(0);
				texcoord.y=(0);
			}
			mb.addTexCoord(texcoord);

			pos=new Vec3();
			pos.x=((verts[f.v3].x)/-1024.0f);
			
			pos.y=((verts[f.v3].y)/-1024.0f);
			
			pos.z=((verts[f.v3].z)/1024.0f);
			mb.addPosition(pos);
			
			//color
			color=new Vec4();
			color.x=(1.0f);//verts[f.v3].getR()/32.0f;
			
			color.y=(1.0f);//verts[f.v3].getG()/32.0f;
			
			color.z=(1.0f);//verts[f.v3].getB()/32.0f;
			
			color.w=(1.0f);
			mb.addColor(color);
			//normals
//			mb.add(1.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			//textcoord
			texcoord=new Vec2();
			if(!f.isColored){
				texcoord.x=((tx*256+(ot.v3.xPixel))/(twidth*256.0f));
				texcoord.y=((ty*256+(ot.v3.yPixel))/(twidth*256.0f));
			}
			else{
				texcoord.x=(0);
				texcoord.y=(0);
			}
			mb.addTexCoord(texcoord);
		}


		//fi=0;
		for(int j=0;j< tricount;j++){

			MeshFace3 f=triangles[j];
			if(f.hasAlphaBlending())
				hasAdditiveBlending=true;
			int t=0;
			ObjectTexture ot=null;
			if (!f.isColored)
			{
				ot=l.levelData.objectTextures[f.getTexture()];
				t = ot.getTile();
			}
			int tx=t%twidth;
			int ty=t/twidth;
			//v0
			mb.addIndex((short)(mb.fi+0));

			mb.addIndex((short)(mb.fi+1));

			mb.addIndex((short)(mb.fi+2));

			mb.fi+=3;



			Vec3 pos=new Vec3();
			pos.x=((verts[f.v0].x) / -1024.0f);

			
			pos.y=((verts[f.v0].y)/-1024.0f);

			
			pos.z=((verts[f.v0].z)/1024.0f);

			mb.addPosition(pos);
			
			
			//color
			Vec4 color=new Vec4();
			color.x=(1.0f);//verts[f.v0].getR()/32.0f;

			color.y=(1.0f);//verts[f.v0].getG()/32.0f;
			
			color.z=(1.0f);//verts[f.v0].getB()/32.0f;
			
			color.w=(1.0f);
			mb.addColor(color);
			
//			//normals
//			mb.add(1.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			//textcoord
			Vec2 textcoord=new Vec2();
			if (!f.isColored)
			{
				textcoord.x=((tx * 256 + (ot.v0.xPixel)) / (twidth * 256.0f));
				textcoord.y=((ty*256+(ot.v0.yPixel))/(twidth*256.0f));
			}
			else{
				textcoord.x=(0);
				textcoord.y=(0);
			}
			mb.addTexCoord(textcoord);

			pos=new Vec3();
			pos.x=((verts[f.v1].x)/-1024.0f);
			
			pos.y=((verts[f.v1].y)/-1024.0f);
			
			pos.z=((verts[f.v1].z)/1024.0f);
			mb.addPosition(pos);
			
			//color
			color=new Vec4();
			color.x=(1.0f);//verts[f.v1].getR()/32.0f;
			
			color.y=(1.0f);//verts[f.v1].getG()/32.0f;
			
			color.z=(1.0f);//verts[f.v1].getB()/32.0f;
			
			color.w=(1.0f);
			mb.addColor(color);
			
			//normals
//			mb.add(1.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			//textcoord
			textcoord=new Vec2();
			if(!f.isColored){
				textcoord.x=((tx*256+(ot.v1.xPixel))/(twidth*256.0f));
			
				textcoord.y=((ty*256+(ot.v1.yPixel))/(twidth*256.0f));
			}
			else{
				textcoord.x=(0);
				textcoord.y=(0);
			}
			mb.addTexCoord(textcoord);

			pos=new Vec3();
			pos.x=((verts[f.v2].x)/-1024.0f);
			
			pos.y=((verts[f.v2].y)/-1024.0f);
			
			pos.z=((verts[f.v2].z)/1024.0f);
			
			mb.addPosition(pos);
			
			//color
			color=new Vec4();
			color.x=(1.0f);//verts[f.v2].getR()/32.0f;
			
			color.y=(1.0f);//verts[f.v2].getG()/32.0f;
			
			color.z=(1.0f);//verts[f.v2].getB()/32.0f;
			
			color.w=(1.0f);
			mb.addColor(color);
			//normals
//			mb.add(1.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			mb.add(0.0f;
//			i++;
//			//textcoord
			textcoord=new Vec2();
			if(!f.isColored){
				textcoord.x=((tx*256+(ot.v2.xPixel))/(twidth*256.0f));
				textcoord.y=((ty*256+(ot.v2.yPixel))/(twidth*256.0f));
			}
			else{
				textcoord.x=(0);
				textcoord.y=(0);
			}
			mb.addTexCoord(textcoord);
		}

		mb.calculateNormals();
		setVertices(mb.genVertices());
		setIndices(mb.genIndices());
		//mVertices=mb.genVertices();
        


		//drawOrder= mb.genIndices();
		
		Random r=new Random();
		mred=r.nextFloat();
		mgreen=r.nextFloat();
		mblue=r.nextFloat();
    }
	List<TRMesh> children=new ArrayList<>();

	public TRMesh getChild(int index)
	{
		return children.get(index);
	}
	public void addChild(TRMesh m)
	{
		//Matrix.multiplyMM(m.mModelMatrix, 0, mModelMatrix,0, m.mModelMatrix, 0);
		children.add(m);
		m.setShader(shader);
	}

	

	@Override
	public boolean hasAddidiveBlending()
	{
		return hasAdditiveBlending;
	}

	public void rotateY(int angle)
	{
		this.roty=angle;
		Matrix.rotateM(mModelMatrix, 0, angle, 0.0f, 1.0f, 0.0f);
//		for(TRMesh c:children)
//			Matrix.multiplyMM(c.mModelMatrix,0,mModelMatrix,0,c.mModelMatrix,0);
	}
	public void rotateZ(int angle)
	{
		this.rotz=angle;
		Matrix.rotateM(mModelMatrix, 0, angle, 0.0f, 0.0f, 1.0f);
//		for(TRMesh c:children)
//			Matrix.multiplyMM(c.mModelMatrix,0,mModelMatrix,0,c.mModelMatrix,0);
	}

	public void rotateX(int angle)
	{
		this.rotx=angle;
		Matrix.rotateM(mModelMatrix, 0, angle, 1.0f, 0.0f, 0.0f);
//		for(TRMesh c:children)
//			Matrix.multiplyMM(c.mModelMatrix,0,mModelMatrix,0,c.mModelMatrix,0);
	}
	public float x,y,z;
	public float lx,ly,lz;
	public void setLocalTranslation(float x, float y, float z)
	{
		this.lx=x;
		this.ly=y;
		this.lz=z;
	}
	
	
	public void translate(float x, float y, float z)
	{
		this.x=x;
		this.y=y;
		this.z=z;
		mModelMatrix[12]=x;
		mModelMatrix[13]=y;
		mModelMatrix[14]=z;
	}

	
	float[] mvpMatrix=new float[16];

	@Override
	public void draw(float[] mvpMatrix)
	{
		//GLES30.glUseProgram(shader.mProgramObject);
//		for(int i=0;i<lights.length;i++){
//			GLES30.glUniform3fv(shader.mLightPosUniform[i],1,lights[i],0);
//			GLES30.glUniform3fv(shader.mLightColorUniform[i],1,lightcolors[i],0);
//		}
		drawme(mvpMatrix,0);
	}

	
    public void drawme(float[] vpMatrix, int depth) {
		for(TRMesh c:children){
			c.translate(x+c.lx,y+c.ly,z+c.lz);
			//Matrix.multiplyMM(c.mModelMatrix,0,mModelMatrix,0,c.mModelMatrix,0);
			c.drawme(vpMatrix,depth+1);
		}
		if(!hasVertices)return;
		GLES30.glUniformMatrix4fv(shader.mMMatrixHandle, 1, false, mModelMatrix, 0);
		Matrix.multiplyMM(this.mvpMatrix, 0, vpMatrix, 0, mModelMatrix, 0);
		super.draw(this.mvpMatrix);
	}
	float[] inversedBindTransform=new float[16];
	public void calcInverseBindTransform(float[] parentBindTransform){
		float[] bindTransform=new float[16];
		float[] localBindTransform = mModelMatrix;
		Matrix.multiplyMM(bindTransform, 0, parentBindTransform, 0, localBindTransform, 0);
		Matrix.invertM(inversedBindTransform,0,bindTransform,0);
		//Matrix4f.invert(bindTransform, inverseBindTransform);
		for (TRMesh child : children)
		{
			child.calcInverseBindTransform(bindTransform);
		}         
	}
}
