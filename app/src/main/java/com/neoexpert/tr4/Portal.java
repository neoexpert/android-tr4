package com.neoexpert.tr4;

import com.neoexpert.math.*;
import com.neoexpert.mesh.*;

public class Portal {
    public final Vec3 v0;
    public final Vec3 v1;
    public final Vec3 v2;
    public final Vec3 v3;
    private float r;
    private float g;
    private float b;
    public Vec3 center;
    public final Plane plane;
    public float radius;

    public final Line l02;
    public final Line l13;
    public final Line l01;
    public final Line l23;

    public Portal(Vec3 v0, Vec3 v1, Vec3 v2, Vec3 v3){
        this.v0=v0;
        this.v1=v1;
        this.v2=v2;
        this.v3=v3;
        Vec3 cv0=v0.subtract(v3);
        Vec3 cv1=v1.subtract(v2);
		Vec3 normal=cv0.cross(cv1).normalize();
        plane=new Plane(v0,normal);
        float avgX=(v0.x+v1.x+v2.x+v3.x)/4f;
        float avgY=(v0.y+v1.y+v2.y+v3.y)/4f;
        float avgZ=(v0.z+v1.z+v2.z+v3.z)/4f;
        center=new Vec3(avgX,avgY,avgZ);
        float radius0 = center.subtract(v0).length();
        float radius1 = center.subtract(v1).length();
        float radius2 = center.subtract(v2).length();
        float radius3 = center.subtract(v3).length();
        radius= FMath.max(radius0,radius1,radius2,radius3);
		this.l02 = new Line(v0, v0.subtract(v2));
		this.l13 = new Line(v1, v1.subtract(v3));
		this.l01 = new Line(v0, v0.subtract(v1));
		this.l23 = new Line(v2, v2.subtract(v3));
    }
	public static boolean l=false;
	public boolean isVisibleFrom(Portal p)
	{
		if (l){
			float d1=-plane.distance(p.center);
			float d2=p.plane.distance(center);
			MainActivity.logger.log(d1 + ">" + -p.radius);
			MainActivity.logger.log(d2 + "<" + radius);
		}
		return -plane.distance(p.center) > -p.radius & p.plane.distance(center) < radius;

	}

    public void setColor(float r, float g, float b){
        this.r=r;
        this.g=g;
        this.b=b;
    };
    public MeshBuilder getMesh(){
        MeshBuilder mb=new MeshBuilder();
        mb.addIndex((short) 0);
        mb.addIndex((short) 1);
        mb.addIndex((short) 1);
        mb.addIndex((short) 2);
        mb.addIndex((short) 2);
        mb.addIndex((short) 3);
        mb.addIndex((short) 3);
        mb.addIndex((short) 0);
        mb.addVertex(v0.x,v0.y,v0.z,1,0,0,1,0,0);
        mb.addVertex(v1.x,v1.y,v1.z,0,1,0,1,0,0);
        mb.addVertex(v3.x,v3.y,v3.z,0,0,1,1,0,0);
        mb.addVertex(v2.x,v2.y,v2.z,1,1,1,1,0,0);
        return mb;
    }

    public PortalFrustum genFrustum() {
        PortalFrustum f=new PortalFrustum(this);
        f.ntl=v3;
        f.ntr=v2;
        f.nbl=v1;
        f.nbr=v0;

        f.ftl=v3;
        f.ftr=v2;
        f.fbl=v1;
        f.fbr=v0;

        Vec3 cv0=v0.subtract(v3);
        Vec3 cv1=v1.subtract(v2);
		Vec3 normal=cv0.cross(cv1).normalize();


        f.topPlane=new Plane(f.ntl,normal);
        f.bottomPlane=new Plane(f.nbl,normal);
        f.rightPlane=new Plane(f.nbr,normal);
        f.leftPlane=new Plane(f.nbl,normal);
        f.nearPlane=plane;
        return f;
		
    }

    public PortalFrustum genFrustum(Portal p) {
        //float avgX=(p.v0.x+p.v1.x+p.v2.x+p.v3.x+v0.x+v1.x+v2.x+v3.x)/8f;
        //float avgY=(p.v0.y+p.v1.y+p.v2.y+p.v3.y+v0.y+v1.y+v2.y+v3.y)/8f;
        //float avgZ=(p.v0.z+p.v1.z+p.v2.z+p.v3.z+v0.z+v1.z+v2.z+v3.z)/8f;
        //Vec3 center=new Vec3(avgX,avgY,avgZ);
        PortalFrustum f=new PortalFrustum(this);
        f.ntl=v3;
        f.ntr=v2;
        f.nbl=v1;
        f.nbr=v0;

        Vec3 cv0=p.v0.subtract(v3);
        Vec3 cv1=p.v1.subtract(v2);
        Vec3 cv2=p.v2.subtract(v1);
        Vec3 cv3=p.v3.subtract(v0);

        f.ftl=p.v0.add(cv0.normalize().multiply(1000f));
        f.ftr=p.v1.add(cv1.normalize().multiply(1000f));
        f.fbl=p.v2.add(cv2.normalize().multiply(1000f));
        f.fbr=p.v3.add(cv3.normalize().multiply(1000f));

        f.topPlane=new Plane(f.ntl,cv0.cross(cv1).normalize());
        f.bottomPlane=new Plane(f.nbl,cv3.cross(cv2).normalize());
        f.rightPlane=new Plane(f.nbr,cv3.cross(cv1).normalize());
        f.leftPlane=new Plane(f.nbl,cv0.cross(cv2).normalize());
        f.nearPlane=plane;
        //f.v0=center;
        //f.v1=center.add(f.leftPlane.getNormal().multiply(100f));
        return f;
		
    }

}
