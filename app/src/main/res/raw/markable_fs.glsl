#version 300 es		 			          	
precision mediump float;
uniform sampler2D u_Texture;    // The input texture.
uniform vec4 markColor;
in vec4 v_Color;
in vec2 v_TexCoord;
out vec4 fragColor;	 	


void main()                                  
{
	vec4 c=v_Color*markColor;
	fragColor =  c *  texture(u_Texture, v_TexCoord);
	if(fragColor.a<0.5)
	discard;                   
}                                            
