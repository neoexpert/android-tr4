#version 300 es 			 
uniform mat4 uMVPMatrix;
uniform mat4 uMMatrix;
in vec4 aPosition;
in vec4 aNormal;
in vec4 aColor;
in vec2 aTexCoord;
out vec4 v_Color;
out vec4 v_Normal;
out vec4 fragPos;
out vec2 v_TexCoord;

void main()                  
{                     
	v_Color = aColor;
	v_Normal=aNormal;
	
	v_Normal.w=0.0;
	v_Normal= transpose(inverse(uMMatrix))* v_Normal;
	
	
	v_TexCoord = aTexCoord;
	//fragPos=aPosition;
	fragPos=uMMatrix * aPosition;
	gl_Position = uMVPMatrix * aPosition;
}
