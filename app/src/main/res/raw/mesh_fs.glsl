
precision mediump float;
uniform sampler2D u_Texture;    // The input texture.

struct Light{
	vec3 pos;
}; 

uniform Light light[LIGHTS_COUNT];

in vec4 v_Color;
in vec4 v_Normal;
in vec2 v_TexCoord;
out vec4 fragColor;	 	
in vec4 fragPos;

void main()                                  
{
	vec4 ambient=vec4(0.0,0.0,0.0,1.0);
	vec3 lightColor=vec3(1.0,1.0,1.0);
	//vec3 lightPos=vec3(-75.0,7.0,36.0);
	
	vec4 diffuse=vec4(0.0,0.0,0.0,1.0);
	for(int i=0;i<LIGHTS_COUNT;i++){
		vec3 lightVec =light[i].pos- fragPos.xyz;
		float dist=length(lightVec)*0.5;
	
	
		vec3 lightDir = normalize(light[i].pos - fragPos.xyz);  
		float diff = max(dot(v_Normal.xyz, lightDir), 0.0);
		diff*=(1.0/(dist*dist));
		diffuse += vec4(diff * lightColor,1.0);
	}
	
	vec4 result = (ambient + diffuse) * (v_Color *  texture(u_Texture, v_TexCoord));
	fragColor =result;
	
	
	
	//vec4 c=v_Color;
	//fragColor =  
	if(fragColor.a<0.5||v_Normal.x>10000000.0)
		discard;                   
}                                            
