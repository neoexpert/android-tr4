#version 300 es 			 
uniform mat4 uMVPMatrix;
in vec4 aPosition;
in vec4 aColor;
in vec2 aTexCoord;
out vec4 v_Color;

out vec2 v_TexCoord;

void main()                  
{                     
	v_Color = aColor;
	
	
	v_TexCoord = aTexCoord;
	gl_Position = uMVPMatrix * aPosition;
}
